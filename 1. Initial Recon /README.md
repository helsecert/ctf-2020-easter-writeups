Oppgavene for Initial Recon er delt i sporene `Subdomener` og `Netflowanalyse`

Oppgavene for `Subdomener` er tiltenkt å gjøres i rekkefølgen:

- [Probing](Probing/OPPGAVE.md)
- [IPv4](IPv4/OPPGAVE.md)
- [Modus operandi](Modus\ operandi/OPPGAVE.md)
- [Programmert melding](Programmert\ melding/OPPGAVE.md)


Oppgavene for `Netflowanalyse` er tiltenkt å gjøres i rekkefølgen:

- [Vertikalt portscan](Vertikalt\ portscan/OPPGAVE.md)
- [En åpen port](En\ åpen\ port/OPPGAVE.md)
- [Kartleggingsverktøy](Kartleggingsverktøy/OPPGAVE.md)
- [Visualisert kode](Visualisert\ kode/OPPGAVE.md)
