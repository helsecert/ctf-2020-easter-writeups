Etter at Påskeharen får høre at angriperen har funnet en åpen port blir han veldig bekymret. Han samler alle påskekyllingene til et kort stand-up møte og oppsummerer at "dette er ikke bra". 

Påskeharen er usikker på om angrepet er målrettet fra en avansert og kompetent aktør, eller om dette bare er noen script-kiddies som tilfeldigvis fant en åpen port.

Kan du gjøre en videre analyse av loggene og finne ut hvilket verktøy og hvilken metode angriper kan ha brukt for å gjennomføre nettverksskanningen?

Påskeharen ønsker svaret levert som "verktøy-metode". Teksten skal være lower case. Metode skal ikke inneholde space. Eksempel: hvis angriper har brukt verktøyet Nessus og kjørt et ping scan så vil svaret være "nessus-pingscan".

Lever `EGG{md5sum av "verktøy-metode"}`

Fortsetter med samme fil som i [Vertikalt portscan](../Vertikalt\ portscan/OPPGAVE.md)  
Fil: [eve-flow.json.7z](../Vertikalt\ portscan/eve-flow.json.7z)

[HINT](HINT.md)