Her er vi ute etter hvilket verktøy som ble benyttet. Det er mulig at det finnes andre verktøy som gjør akkurat dette, men vi er ute etter et veldig populært og spesifikt verktøy. 

Det som er give-away er antall porter (1000) og hvilke porter. Diverse google søk etter de 10 siste portene i scannet (sortert) + 1000 + scanner vil si at dette er `nmap`. For å liste opp portene kan vi bruke `jq`:
```
cat eve-flow.json | jq ".dest_port" | sort -n | uniq |tr '\n' ',' 
```

Videre så må vi se på teknikken som er brukt. Dataene er de samme som for oppgave 2 `En åpen port`. Siden vi ser SYN->  og <-RST/ACK for lukkede porter, og SYN, SYN/ACK, RST for åpne porter så gir det hint til at dette er et SYN scan. Kan også hete "stealth" scan med mer. Vi er ute etter `synscan`

Svaret er derfor `nmap-synscan`

```
$ echo -n "nmap-synscan" | md5sum
5848022eb217aa2f6b457a09dfddb067 
```

Flagg: `EGG{5848022eb217aa2f6b457a09dfddb067}`
