Påskeharen liker ikke tanken på at angriper har brukt Nmap. Dette kan tyde på at vi har en litt over middels interessert motstander. Verktøyet krever noe kompetanse i bruk, og det virker som denne angriperen kan å bruke det. Påskekyllingene har derfor trappet opp søk etter tilsvarende trafikk.

Vedlagt ligger et nytt uttrekk av netflow-data fra Suricata. Det er oppdaget et merkelig mønster i trafikken fra en kilde-adresse mot en av våre servere. 

Påskeharen mistenker at angriper forsøker å finne et EGG. Han bare ler av forsøket, siden det ikke er gjemt noen kode til et EGG på akkurat denne serveren. Han ønsker likevel å finne ut hvilken kode angriper forsøker å få tak i.

Greier du å finne koden?

Lever `EGG{<32-byte hex>}`

Fil: [eve-flow2.json.7z](eve-flow2.json.7z)

[HINT](HINT.md)
