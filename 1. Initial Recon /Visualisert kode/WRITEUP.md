Det eneste som varierer mellom logg-linjene er timestamp, dest_port, flow.start og flow.end. Det er mange hint om at man må visualisere dataene.

Hvis man kun ser på loggene for UDP så vil man oppdage at dest_port korresponderer med ASCII verdier, og man får ut et hint:

`Bra! Men dessverre fant du bare et EGG{ } uten innhold -- hva med visualisering av TCP-pakkene?`

Det vi er ute etter er at man lager en graf av TCP loggene med timestamp/flow.start som x-akse og dest_port som y-akse. Dette kan gjøres både i python og excel/google docs. Dette bygger et `scatter plot`, og kan se slik ut:

![scatterplot](del4_graph.PNG "scatterplot tid/dest_port")

Denne teknikken kan benyttes med Netflow data for å oppdage både horisontale og vertikale portscan, samt portscan som bruker _svært_ lang tid (dager, uker, måneder).

Flag: `EGG{97525DA5FF1DAD557E53AAAF79C03E64}`