Vi ønsker å lære mer om angripers modus operandi, dvs. måten angriper arbeider på. Dette kan være spesielle ting angriper gjør eller rekkefølgen på hvordan ting gjøres. Hvis dette er unikt nok så kan det brukes til å identifisere og korrelere annen aktivitet fra samme angriper.

Påskeharen ønsker å sette sammen subdomenene til et fingeravtrykk. For å gjøre det mer unikt så ser vi kun på domener som ikke har satt Recursion Desired bit (norecurse) i spørringen. Fingeravtrykket blir bedre hvis domenene er sortert, og hvis de er unike.

Kan du lage et fingeravtrykk av spørringene?

Lever `EGG{<md5sum av <domene1><domene2><domene3><...>>}`

<b>Oppdatering</b>: Vi ønsker unike, sorterte subdomener av journalsystem.ctf OG journelsystem.ctf som er gjort uten "recursion desired"-bit satt. Eks.: md5sum("aaa.journelsystem.ctfbbb.journalsystem.ctf").

<b>Oppdatering</b>: sort kan gi ulikt resultat ut fra LC_COLLATE, og ikke nødvendigvis versjonen av sort. Oppgaven ble laget på en datamaskin LC_COLLATE="en_US.UTF-8" og sort (GNU coreutils) 8.31

Fortsetter med samme fil som i [Probing](../Probing/OPPGAVE.md)
Fil: [eve-flow.json.7z](../Probing/eve-dns.json.7z)

[HINT](HINT.md)