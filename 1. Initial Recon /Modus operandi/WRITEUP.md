I denne oppgaven ble det mange som nok fant svaret, men som ikke fikk levert flagget pga varierende resultat fra `sort`, avhengig av LC_COLLATE, operativsystem og sikkert også andre årsaker. Det ble etterhvert lagt inn 28 flagg som riktig svar (permuteringer av LC_COLLATE).

Det vi var ute etter her er å filtrere bort alle meldinger som har `rd` (recursion desired) satt. Avhengig av hvor man kjører dette, så vil man da få en rekkefølge på dataene og hashet vil variere ut fra dette.

På datamaskinen til han som lagde oppgaven får man ut dette:
```
$ cat eve-dns.json  | jq "select(.dns.type==\"answer\") | select(.dns.rd!=true) | .dns.rrname" | sed 's/"//g' | sort | uniq | tr '\n' '#' | sed 's/#//g' | md5sum 
2e6eece2e0189ab6a38ea56e79a4cf23
``` 

Flagg: 

EGG{2e6eece2e0189ab6a38ea56e79a4cf23}

EGG{b1c9429a1a0286e1dcc1b306a6c3fc90}

EGG{0696ae29a133932b5dc14de363cfd5d4}

EGG{10a7a21d8611b9734d0e7b3cff1d1692}

EGG{11c7a70b9fbeede874510417dc641b3a}

EGG{134774ac0bf90f7fd837cd17dff8ce6a}

EGG{38a2fe05ec89b036b2f5c081e803f928}

EGG{3effbc9a43a6b9816c2c12147a25f763}

EGG{43064eda990108f18fa209c9a970ff79}

EGG{4478347f2ac363ebc9e62b64e634637c}

EGG{5685f1b5528f1d0bff0cc6dae1382d60}

EGG{5a8f4e3c09c7ee5fdabba10f2343e4fd}

EGG{62782a7bb2f92ad7974c7f14f2ee3c1b}

EGG{742127b2a550d95f22d0535b7d2314b0}

EGG{84e0e33d300184188ad41ae63040f771}

EGG{86a00237b458308f7c7385868abed372}

EGG{8e04190cafca32070f1180565a6c5e2c}

EGG{b1c9429a1a0286e1dcc1b306a6c3fc90}

EGG{b476676980f4d20badbaf9d80cc839e3}

EGG{b8d40b58e14f76c4b3052c2fd7bf73c5}

EGG{ba17ef95dc1a3478dbeb7c4c05face86}

EGG{c20ee9c9c02e7c668e90fb59f5302f88}

EGG{e7b37617e2c02c0bb97f56e888b97fe2}