Påskeharen frykter at angriperen har lyktes med sitt nettverkskartleggingsforsøk. Under en intern diskusjon i analysesentret sier han til påskekyllingene: – Jeg håper angriper ikke har funnet en åpen port.

Alle påskekyllingene vet at en åpen port potensielt kan utnyttes til å gjennomføre et angrep og potensielt kompromittere en datamaskin. 

Kan du analysere loggen fra nettverksskannet og se om du finner noen åpne porter?

Lever `EGG{<md5sum av portnummer>}`

Fortsetter med samme fil som i [Vertikalt portscan](../Vertikalt\ portscan/OPPGAVE.md)  
Fil: [eve-flow.json.7z](../Vertikalt\ portscan/eve-flow.json.7z)

[HINT](HINT.md)