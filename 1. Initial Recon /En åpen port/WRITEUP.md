Løsningen er å finne porten som har svart med at den er åpen. Kort teori om SYN scanning:

## Scan open port:
```
SYN
-->

SYN/ACK
<------

RST
-->

(flags_ts) Klient -> Server: 0x06 => 00000110 (SYN, RST)
(flags_tc) Server -> Client: 0x12 => 00010010 (SYN, ACK)
```

## Scan closed port:
```
SYN
-->

RST/ACK
<------

(flags_ts) Klient -> Server: 0x02 => 00000010 (SYN)
(flags_tc) Server -> Client: 0x14 => 00010100 (RST, ACK)
```

## Løsning del2

Vi leter altså etter en sesjon som har flags_ts (to server) 0x06 (SYN,RST) og flags_tc (to client) 0x12 (SYN,ACK).

Alternativt er bare å liste opp flags_ts og flags_tc så er det kun 1 par som viser den som er åpen.

```
$ cat eve-flow.json | grep "TCP" | jq "[.tcp.tcp_flags_ts, .tcp.tcp_flags_tc, .dest_port]" -cr | sort | tail -n1
["06","12",5822]

# echo -n "5822" | md5sum 
fd2ae8ec902471d8956fca3486031013  -

```

Flagg: `EGG{fd2ae8ec902471d8956fca3486031013}`