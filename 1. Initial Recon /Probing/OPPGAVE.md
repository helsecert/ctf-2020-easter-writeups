En av våre flittige påskekyllinger har oppdaget et sub-domene scan ved å analysere <a href="/info/passiv_DNS">passiv DNS</a>-data fra Suricata. Det virker som noen forsøker å finne subdomener ved å skanne med en ordliste. Kartleggingen er ganske omfattende og påskekyllingen har ikke funnet ut hvor mange unike subdomener av `journalsystem.ctf` som er (forsøkt eller vellykket) kartlagt.

Kan du laste ned vedlagt passiv DNS data og komme med et svar?

Lever `EGG{<md5sum av antall unike subdomener>}`

Fil: [eve-dns.json.7z](eve-dns.json.7z)

[HINT](HINT.md)