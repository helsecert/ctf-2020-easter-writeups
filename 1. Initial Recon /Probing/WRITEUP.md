Finne antall unike subdomener, uavhengig av hva som spørres etter. Det må filtreres på journalsystem.ctf fordi det scannes etter en skrivefeil i domenet.

```
$ cat eve-dns.json  | jq "select(.dns.type==\"query\") | .dns.rrname" -cr | grep "journalsystem.ctf"  | sort | uniq | wc -l

$ echo -n "9338" | md5sum 
7b3403f79b478699224bb449509694cf  
```

Flagg: `EGG{7b3403f79b478699224bb449509694cf}`