Her var vi ute etter antall destinasjonsporter. Disse ligger i ".dest_port" feltet i JSON. Kan løses med `jq`

```
cat eve-flow.json | jq ".dest_port" | sort -n | uniq | wc -l
1000

$ echo -n "1000" | md5sum
a9b7ba70783b617e9998dc4dd82eb3c5
```

Flagg: `EGG{a9b7ba70783b617e9998dc4dd82eb3c5}`