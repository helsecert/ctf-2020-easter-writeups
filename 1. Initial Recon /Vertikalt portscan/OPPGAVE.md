En påskekylling har akkurat kvittert en alarm med kritikalitet HØY som dukket opp på skjermen. Alarmen kommer fra et av de mange avanserte innbruddsystemene som analysesentret har tilgang på: Suricata. Alarmen viser at noen har kjørt et vertikalt portskann mot en av serverne som er eksponert ut mot internett.

Selv om denne påskekyllingen startet som en cyberoperatør i forrige uke er han godt kjent med portskanning. Etter å ha sett litt på alarmen gjør han derfor et uttrekk av tilhørende <a href="/info/netflow">netflow data</a>. Dette viser metainformasjon om den bidireksjonale (toveis) flyten av pakker mellom angriper og vår server.

Sjefsanalytikeren ønsker en rapport på hvor mange unike destinasjonsporter som angriper har forsøkt kartlagt. Kan du hjelpe påskekyllingen?

Lever `EGG{<md5sum av antall unike destinasjonsporter>}`

Fil: [eve-flow.json.7z](eve-flow.json.7z)

[HINT](HINT.md)