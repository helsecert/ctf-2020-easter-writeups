I de tidligere oppgavene har man oppdaget at det er en del TXT events. TXT er et felt i DNS som ofte benyttes for å overføre informasjon. Dette henger også sammen med de to domenene som ble funnet i `IPv4` oppgaven:

```
adminportalXXXX.journalsystem.ctf
webfront01.journalsystem.ctf
```

TXT record for `webfront01` er `argument=Dkf5qk64zvZ5DEsAFnvTBQC81BZrmwJoQJ1a1Wt6XfP4` og virker å være et eller annen enkodet argument. Hvis du forsøker i CyberChef så vil du ikke få ut noe der, verken på base58 eller base64. Kommer tilbake til hvorfor lengre nede.


TXT records for `adminportalXXXX` inneholder binærdata. Rekkefølgen på disse er tilfeldig, men de har en struktur. For de som har brukt verktøy som `hexdump` så kan dette virke kjent. Du må ta ut TXT svaret for hvert domene som er spurt og sortere ut fra slutten av domenet (hexadecimalt). Deretter kan vi skrive innholdet til en fil. Her lister vi ut domenet og TXT svaret.

```bash
cat eve-dns.json  | jq "select(.dns.type==\"answer\") | select(.dns.rrtype==\"TXT\") | select(.dns.rrname!=\"webfront01.journalsystem.ctf\") | .dns.answers[0] | [ .rrname, .rdata ]" -rc

["adminportal02e0.journalsystem.ctf","48 28 f3 b2 50 ad b9 c3 c6 c7 c6 b0 ab c9 09 39"]
["adminportal00f0.journalsystem.ctf","fb 5d 13 56 08 5e 3a f2 a3 a7 b2 5a 80 ca 8d f7"]
["adminportal0370.journalsystem.ctf","ea 05 c6 7e 96 bc 88 55 ef 0a 00 91 85 8c 53 ec"]
["adminportal0380.journalsystem.ctf","3e 6a 91 f0 24 04 32 42 b8 b0 26 20 c0 64 0d e7"]
["adminportal06e0.journalsystem.ctf","14 86 58 8d 1c 94 3b e2 42 87 36 75 09 3a 10 5a"]
....
(klippet vekk resten)
```

Vi ønsker å sortere på domene, men kun ta ut TXT records. Vi vil da se en binærfil i hexadecimal form. Vi skriver alt til ei fil.

```bash
cat eve-dns.json  | jq "select(.dns.type==\"answer\") | select(.dns.rrtype==\"TXT\") | select(.dns.rrname!=\"webfront01.journalsystem.ctf\") | .dns.answers[0] | [ .rrname, .rdata ]" -rc | sort | uniq | jq ".[1]" | sed 's/\"//g' | sed 's/ //g' | grep -v null > binary

cat binary | head -n 4
89504e470d0a1a0a0000000d49484452
000002080000013601000000002c1f57
8e0000108349444154789ced9c5f6c1c
c77dc73ffba7da554b1dd769029c9b33
```

Nå gjør vi om dette til en faktisk binær fil. For de som har et trent øye så ser man at binærfilen starter med `8950 4e47` (osv) altså PNG headers.

```
xxd -r -p binary binary.png

file binary.png
binary.png: PNG image data, 520 x 310, 1-bit grayscale, non-interlaced

eog binary.png
```

![binary](binary.png "binary.png")

Ved å skrive ned python koden og kjøre den med argumentet fra TXT record for webfront01 finner vi flagget.
```python
import binascii
import sys
a = '123456789ABC'
a += 'DxFyHJKLMNPQRSTU'
a += 'VWXYZabcdefzhijkmnopqr'
a = a + 'stuvwEGg'
a = a[:13] + a[55] + a[14:55] + a[13] + a[56:]
a = a[:15] + a[56] + a[16:56] + a[15] + a[57:]
a = a[:39] + a[57] + a[40:57] + a[39] + a[58:]
def d(s):
    n = 0
    for c in s:
        n = n*len(a) + a.index(c)
    return binascii.unhexlify(('%x' % n).encode('utf-8'))
print(f"EGG{{{d(sys.argv[1][::-1]).decode('utf-8')}}}")
```

```bash
python binary.py Dkf5qk64zvZ5DEsAFnvTBQC81BZrmwJoQJ1a1Wt6XfP4
```

Koden er en ned-strippet versjon av base58 decode. Som man ser i `sys.argv[1][::-1]` så blir input reversert. Det er derfor mulig å løse denne i CyberChef, hvis man snur `Dkf5qk64zvZ5DEsAFnvTBQC81BZrmwJoQJ1a1Wt6XfP4` også kjører base58 decode :-)

Flagg: EGG{2adcaf3959418e0a8d754c41f4cad64b}
