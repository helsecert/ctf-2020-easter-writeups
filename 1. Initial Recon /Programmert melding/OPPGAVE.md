En cyberoperatør i analysesentret har oppdaget et rart mønster i oppslagene mot noen av subdomenene. Det kan virke som angriper forsøker å hente ut en kodet melding. Flere påskekyllinger har sett på trafikken og gjort avanserte analyser, men uten resultater. 

Finner du en melding i loggene?

Lever `EGG{<32-byte hex>}`

Fortsetter med samme fil som i [Probing](../Probing/OPPGAVE.md)
Fil: [eve-flow.json.7z](../Probing/eve-dns.json.7z)

[HINT](HINT.md)