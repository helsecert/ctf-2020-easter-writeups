Her skal vi finne domener som har svart (dvs NOERROR rcode ellers answers har et svar) og som er A record (ipv4).

DNS return codes
```
 NOERROR 	RCODE:0 	 DNS Query completed successfully   
 SERVFAIL 	RCODE:2 	 Server failed to complete the DNS request
 NXDOMAIN 	RCODE:3 	 Domain name does not exist.  
```

DNS RRTYPE
```
  A         Address record      Returns a 32-bit IPv4 address, most commonly used to map hostnames to an IP address of the host.
```

Vi finner at det er gjort 2 svar:
```
cat eve-dns.json  | jq "select(.dns.type==\"answer\") | select(.dns.rcode==\"NOERROR\") | select(.dns.answers[0].rrtype==\"A\") | .dns.answers[0].rrname" | sed 's/"//g'
adminportal.journalsystem.ctf
webfront01.journalsystem.ctf
```

Disse setter vi sammen og får ut md5sum:
```
$ echo -n "adminportal.journalsystem.ctfwebfront01.journalsystem.ctf" | md5sum
17395d6abdbc5482cf852c2712865cd1  -
```

Vi kan også kjøre alt i samme kommando:
```
$  cat eve-dns.json  | jq "select(.dns.type==\"answer\") | select(.dns.rcode==\"NOERROR\") | select(.dns.answers[0].rrtype==\"A\") | .dns.answers[0].rrname" | sed 's/"//g' | sort | uniq | tr '\n' '#' | sed 's/#//g' | md5sum
17395d6abdbc5482cf852c2712865cd1
```

Flagg: `EGG{17395d6abdbc5482cf852c2712865cd1}`