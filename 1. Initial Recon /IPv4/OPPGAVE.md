Angriper har åpenbart forsøkt mange sub-domener. Det er fremdeles uklart om noen av disse har gitt et svar. Dette er noe en av påskekyllingene i analysesentret forsøker å finne ut. Han er ikke så god på DNS og trenger litt hjelp. Påskekyllingen hadde tenkt å sette sammen domenene for å lage en 32-byte hex kode. Han tar kun med domener som gir svar og som svarer med en resource record type for en 32-bits adresse.

Kan du analysere loggfilen og se om du finner domenene? 

Lever `EGG{<md5sum av <domene1><domene2><domene3><...>>}`

Domenene skal være sortert (på ASCII-verdi) og settes sammen uten mellomrom. 

Eksempel: Hvis du finner foobar.example.org og barfoo.example.org så vil strengen være `barfoo.example.orgfoobar.example.org`

Fortsetter med samme fil som i [Probing](../Probing/OPPGAVE.md)
Fil: [eve-flow.json.7z](../Probing/eve-dns.json.7z)

[HINT](HINT.md)