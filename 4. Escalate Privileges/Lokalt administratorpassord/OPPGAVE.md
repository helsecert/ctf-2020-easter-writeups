Angriperne har benyttet en sårbarhet i en nettverkstjeneste til å dumpe prosessminnet til lsass.exe. Fra minnedumpen, som du finner i filen lsass.dmp, mistenkes det at kan man finne lokalt administratorpassord.

Kan du sjekke om det er mulig?

Lever `EGG{md5sum av passord}`

FIL: [lsass.dmp.7z](lsass.dmp.7z)

[HINT](HINT.md)