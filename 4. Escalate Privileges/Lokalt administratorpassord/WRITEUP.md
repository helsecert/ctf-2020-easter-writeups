Du får utdelt en minnedump av prosessen lsass.exe. Denne prosessen er ansvarlig for blant annet autentisering av brukere. Mimikatz er et Windows-program som blant annet kan lese minnedump av lsass.exe. I senere tid har man også fått pypykatz som også fungerer i Linux.

Løsning
1. Installer pypykatz
2. Kjør pypykatz `$ pypykatz lsa minidump lsass.dmp`
3. Finn passordet i output.

Lenke til LSASS på Wikipedia: https://en.wikipedia.org/wiki/Local_Security_Authority_Subsystem_Service

Lenke til mimikatz: https://github.com/gentilkiwi/mimikatz

Lenke til pypykatz: https://github.com/skelsec/pypykatz

Flagg: `EGG{md5(NebbeteKylling2020)}` == `EGG{d45cd0ccc776b7aa639d4d378936d78b}`

