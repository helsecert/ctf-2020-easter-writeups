Niels Henrik Hare er Påskeharens kollega. Han er egentlg matematiker og liker finurlige matematiske utfordringer. Etter et langt forskningsopphold på Large Hash Collider i Sveits har han nå flyttet hjem og fått seg jobb hos Påskeharen.

Han vil gjerne at passord skal hashes med en god KDF som for eksempel Argon2, bcrypt eller scrypt, men på jobb finnes det mange gamle legacysystemer med dårlig passordhashing. For å illustrere viktigheten av salting og sterke passordhashingfunksjoner har han begynt å bruke forskjellige passord, men samme passordhash på alle interne systemer. Passordene hans består også bare av store bokstaver.

Dessverre ser det ut som angriperne har funnet ut av dette og misbrukt en av hans kontoer for å logge på maskinen som styrer Påskeharens ventilasjonsanlegg.

Av og til vil nemlig den varierende luftstrømmen fra ventilasjonsanlegget harmonere med egenfrekvensen til Niels Henriks værhår. Det blir fryktelig ubehagelig, spesielt når det er lavtrykk, og derfor har Niels Henrik fått tilgang til å overstyre viftehastigheten manuelt.

Angriperne henta ut passordhashen hans på det interne saksbehandlingssystemet. Passordet var MWCTSQWA og var hashet med MD5 (uten salting, så klart). Ventilasjonsanlegget ble kjøpt brukt i 1999 og styres fortsatt fra den samme gamle Windows NT-maskinen som fulgte med.

Hvilket passord bruker Niels Henrik på Windows NT-maskinen?

Lever `EGG{md5sum av syv første tegn i passordet}`

[HINT](HINT.md)