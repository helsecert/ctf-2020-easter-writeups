Påskeharen bruker samme passordhash på alle systemer. Det ene systemet har passord hashet (usaltet) med MD5. Ventiasjonssystemet fra 90-tallet kjøres på en maskin som lagrer både LM-hasher og NT-hasher (ofte kalt NTLM-hasher). Det er LM-hashen(e) vi bryr oss om her. At vi kun skal ha de 7 første tegnene er også et hint om dette.

## Løsning

Vi får oppgitt passordet `MWCTSQWA` og kan lage en MD5-hash av dette `d4536e6f7f62f2f8a04a49b3da8caab6` som vi lagrer i filen hash.lm.

Vi kjører John the Ripper (eller hashcat) på hash.lm: `$ john --format=lm --incremental hash.lm`

Og får ganske fort ut løsningen: `AAGDUTD`

MD5 av dette gir flagget.

Det kom noe kritikk om at LM-hasher egentlig bare er 64 bits, og at det lagrede passordet er to LM-hasher, altså kan ikke hashen være lik en MD5-sum (som er 128 bits). Vi tar til oss kritikken og skal være tydeligere neste gang. :-)

Flagg: `EGG{a616e85df4434de079548fbc9806d08c}`
