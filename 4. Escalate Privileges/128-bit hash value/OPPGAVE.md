Et kritisk system for risikovurdering hos Påskeharen har gått i lås. Det er kun kylling62 som har administratortilgang, og hun er på utenlandsreise. Hun vil ikke oppgi passordet på telefon, men sier at de fem siste passordene for dette systemet ligger trygt i Påskeharens passordsafe.

Sikkerhetssjefen sjekker passordsafen, ser gjennom passordene, og finner følgende oppføringer:

<pre>
Hosten2016!
Vinter2017!
Advent2018!
Sommer2019!
</pre></br>

Den siste og relevante oppføringen ser det ut som er en passordhash og brukerinfo i stedet for et passord:
`kylling62:$1$Etg2ExUZ$JXufKC/8AIONHTSYefpTG0:16403:0:99999:7:::`

Sikkerhetssjefen rynker kraftig på nesen over passordvalgene, og tenker at her må det inn litt opplæring i generering av sterke passord, samt tekniske tiltak for å nekte ansatte å sette slike passord.

Samtidig har han nå store forhåpninger til å komme seg inn med administratorbrukeren. Hans kollega kryptosjefen har nemlig bygd en egen datamaskin for å knekke passordhasher. Problemet er at den brukes av flere i analysesentret og derfor har gått varm. En ivrig påskekylling har kastet snø på skjermkortet for å kjøle det ned og nå er datamaskinen ødelagt.

Kan du finne passordet til kylling62?

Lever `EGG{md5sum av passordet}`

[HINT](HINT.md)