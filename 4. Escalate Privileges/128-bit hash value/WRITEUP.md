Her handler det om å forstå oppgaven.

I oppgaven har vi oppgitt et passordhash og brukerinfo på formatet: `<brukernavn>:<hash>:<masse tall>`. Hvis man hadde brukt Google fant man kanskje en nettside:
https://www.slashroot.in/how-are-passwords-stored-linux-understanding-hashing-shadow-utils

Her får man forklart hvordan dette er bygd opp, veldig kort: i `<hash>` delen så er det 3 biter, separert med `$`:
- `1` betyr at det er md5 (crypt)
- `Etg2ExUZ` er saltet
- `JXufKC/8AIONHTSYefpTG0` er hashet av `salt+passord`.

Du må knekke passordet for å løse oppgaven. 

Salting av passord går ut på at man tilfører noe før man gjør hashingen av passordet, altså legger til noe data før eller etter passordet. Hva som tilføres er ikke spesielt viktig, men det bør ha litt lengde (> 4 byte). Ofte kan brukernavnet benyttes som salt. Saltet i seg selv er ikke hemmelig, og det må lagres sammens med brukernavnet og passord-hashet. Salting av passord gjør at det blir nærmest umulig å gjøre "oppslag" av hash for å finne riktig passord med tabeller (e.g. rainbow). 

Vi ser at passordene som er satt fra før er 6 bokstaver, 4 tall og et spesialtegn. Hvis vi skal knekke dette fullt ut, med salting, så vil det ta en del tid. Vi kan bruke kategorier av tegn i passord for å skrive passord-masker:

- `u` (upper/store bokstaver) 29 tegn (A-Zæøå)
- `l` (lower/små bokstaver) 29 tegn (a-zæøå)
- `d` (digit/tall) 10 tall (0-9)
- `s` (spesialtegn) 32 tegn (!-?" ... osv)

Passordet har altså masken: `ullllldddds`. Full knekking av dette betyr at vi må sjekke `190 343 462 720 000` passord. En normal laptop med en GPU greier ca 200 millioner hash per sekund, og dette vil derfor ta nesten 2 døgn. `Vi bør forsøke å redusere hvor mange passord som må forsøkes` (search-space).

Ut fra de tidligere passordene kan vi med rimelig sikkerhet si at årets passord slutter på `2020!`. Passordet vi da skal knekke vil ha masken `ulllll2020!`, altså kun 6 ukjente tegn, som gir `594 823 321` search-space. På vår laptop tar dette kun sekunder.

Uten å vite noe om passordet på forhånd kunne vi risikere å måtte knekke et 11-bokstavs passord med 101 tegn. Det ville tatt ca 176 000 år på vår laptop. Dette viser viktigheten av å redusere search-space når man gjør knekking, og hvor viktig det er å ha god nok lengde på passord for å unngå nettopp knekking -- samt brukte salting.

Det er mange verktøy som kan benyttes. Med `hashcat` finner vi raskt at passordet er `Vinter2020!`. Flagget er en md5sum av dette.

```bash
$ echo "$1$Etg2ExUZ$JXufKC/8AIONHTSYefpTG0" > hash.txt
$ hashcat -m 500 -a 3 hash.txt '?u?l?l?l?l?l2020!'
$1$Etg2ExUZ$JXufKC/8AIONHTSYefpTG0:Vinter2020!        <--- resultatet fra hashcat

$ echo -n 'Vinter2020!' | md5sum
807aa99bb10f0d4a6647a68f8c1abea1
```

Flagg: `EGG{807aa99bb10f0d4a6647a68f8c1abea1}`
