Oppgavene for `Esclate Privileges` har følgende tiltenkte rekkefølge:

- [128-bit hash value](128-bit hash value/OPPGAVE.md)
- [Lokalt administratorpassord](Lokalt\ administratorpassord/OPPGAVE.md)
- [Large Hash Collider](Large\ Hash\ Collider/OPPGAVE.md)

