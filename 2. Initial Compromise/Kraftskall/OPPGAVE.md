Enda en bruker har sendt en forespørsel. Det er kommet inn et nytt dokument som vedlegg til en e-post.

Brukeren skryter av at sikkerhet på e-post er enkelt, og at både mail-headers eller innholdet i e-posten virker å være normalt. Det kan virke som denne brukeren har hatt kursing på e-post sikkerhet. Brukeren sier også at det er sjekket etter lenker som kan virke mistenkelige i e-posten, og det er det ikke.

Siden e-posten er åpnet, og virker OK så lurer brukeren på om det er trygt å åpne dokumentet.

Kan du finne ut om det er trygt å åpne dette?

OBS: Samme advarsel (og passord) som forrige oppgave.

Lever `EGG{32-byte hex}`

FIL: [2_Your_Receipt_1146.7z](2_Your_Receipt_1146.7z)

[HINT](HINT.md)