Nok en e-post har kommet med enda et vedlegg. Avsender begynner kanskje også å bli irritert siden ingen av vedleggene blir kjørt av bruker.

Vår bruker orker ikke å sjekke e-posten selv, og har bare videresendt vedlegget til analysesentret for en sjekk.

Kan du se om du finner noe EGG i dokumentet?

OBS: Samme advarsel (og passord) som forrige oppgave.

Lever `EGG{32-byte hex}`

FIL: [4_Paid_Receipt_2308.7z](4_Paid_Receipt_2308.7z)

[HINT](HINT.md)