## Writeup: En uheldig eggejakt

### Oppgavetekst og hint
![Nok en e-post har kommet med enda et vedlegg. Avsender begynner kanskje også å bli irritert siden ingen av vedleggene blir kjørt av bruker. Vår bruker orker ikke å sjekke e-posten selv, og har bare videresendt vedlegget til analysesentret for en sjekk. Kan du se om du finner noe EGG i dokumentet? OBS: Samme advarsel (og passord) som forrige oppgave. Lever EGG{32-byte hex}](imgs/eggejakt_1.png "Nok en e-post har kommet med enda et vedlegg. Avsender begynner kanskje også å bli irritert siden ingen av vedleggene blir kjørt av bruker. Vår bruker orker ikke å sjekke e-posten selv, og har bare videresendt vedlegget til analysesentret for en sjekk. Kan du se om du finner noe EGG i dokumentet? OBS: Samme advarsel (og passord) som forrige oppgave. Lever EGG{32-byte hex}")  
Etter to dager ble også hint for "en uheldig eggejakt" lagt ut.  
![Hint: All you need is ... a string?](imgs/eggejakt_2.png "Hint: All you need is ... a string?")  

### Manuell løsning

1. Kjører file for å sjekke filtype. Dette forteller oss ikke noe nytt. Dette er også et composite document. 

2. Vi bruker deretter olevba fra [oletools](https://github.com/decalage2/oletools) for å sjekke etter macroer.
 Skjermdumpen inneholder en del av makroen i dokumentet her. Det er ganske mye. Men om vi ser litt nøyere på resultatet ser vi at det er snakk om to typer. Den ene typen er den som slutter hver tilordning med `ChrB()` gjerne inni select-blokker. Den andre typen bygger sammen stringer som kan se ut som om de er base64-compliant.
 ![Vi ser at det er mye støy i makroen i dette dokumentet](imgs/2020-04-21_12-30.png "Vi ser at det er mye støy i makroen i dette dokumentet")
 AutoOpen er en fin plass å finn ut hva som faktisk er i bruk i makroen. Vi ser at det feineres en streng litt under, og at de bruker feilhåndtering for å "feile" seg ned dit.
 ![Sub AutoOpen er starten på eksekvering så det er en god plass å begynne å se hva som skjer.](imgs/2020-04-21_12-36.png "Sub AutoOpen er starten på eksekvering så det er en god plass å begynne å se hva som skjer.")
 Ser vi på makrokode oppe igjen ser vi hvilke strenger som er relevante.
 ![Enkelte av linjene definerer strenger. Disse er interresante](imgs/2020-04-21_12-32.png "Enkelte av linjene definerer strenger. Disse er interresante")
 

3. Med dette chainer vi et par kommandoer for å hente ut resultatene vi ønsker og pynte disse litt.
 ```
 $ olevba -c Paid_Receipt_2308.doc | grep '" + "' | sed 's/" + "//g' | cut -d "=" -f 2 | tr '\n' ' ' | sed 's/"  "//g' | sed 's/"//g' 
 ```
 ![Vi ser at vi får ut resten av powershell og en base64-blob](imgs/2020-04-21_13-18.png "Vi ser at vi får ut resten av powershell og en base64-blob")
 > grep '" + "' --> For å hente ut alle linjer som bygger disse strengene
 > sed 's/" + "//g' --> For å slå sammen strengene
 > cut -d "=" -f 2 --> For å hente ut dataen til høyre for likhetstegnene.
 > tr '\n' ' ' --> Fjerne newlines og slå alt sammen til en linje
 > sed 's/"  "//g' --> Fjerne gjenværende mellomrom mellom strengene
 > sed 's/"//g' --> Og gjenværende fnutter
 
 Vi ser at vi får ut resten av powershell og en base64-blob.   
4. Siden vi bare trenger payload kutter vi bort første del av linjen og dekoder base64. 
  ```
  olevba -c Paid_Receipt_2308.doc | grep '" + "' | sed 's/" + "//g' | cut -d "=" -f 2 | tr '\n' ' ' | sed 's/"  "//g' | sed 's/"//g' | cut -d " " -f 4- | base64 -d | sed "s/\x00//g" | sed "s/'+'//g"
  ```
 Dette begynner å ligne på noe, men vi har enda litt gjenværende obfuskering i form av oppsplitting av strenger med ´'+'´.
 ![Dette begynner å ligne på noe, men vi har enda litt gjenværende obfuskering](imgs/2020-04-21_13-24.png "Dette begynner å ligne på noe, men vi har enda litt gjenværende obfuskering")
 


5. Siden teksten i base64-bloben er wide-strenger fjerner vi nullbytes, og slår deretter sammen strengene.
 ```
 $ olevba -c Paid_Receipt_2308.doc | grep '" + "' | sed 's/" + "//g' | cut -d "=" -f 2 | tr '\n' ' ' | sed 's/"  "//g' | sed 's/"//g' | cut -d " " -f 4- | base64 -d | sed "s/\x00//g" | sed "s/'+'//g"
 ```
 ![Og der har vi EGGet](imgs/2020-04-21_13-28.png "Og der har vi EGGet")
 EGGet er funnet!

 Denne oppgaven er bygd på maldocs sendt ut av gruppen som står bak Emotet. Disse maldocsene inneholder 5 domener, og makroen vil forsøke å laste ned fra en etter en fram til den finner et domene som er aktivt. Obfuskeringen er helt lik den som på et tidspunkt ble brukt av Emotet. Maldocsene de sender ut er (som regel) mye bedre obfuskert enn det som brukes av de fleste APT-grupper. (Noe som har mange underliggende grunner.)

### Verktøyløsning

Selv med all obfuskeringen må makroen kunne eksekvere for at det skal være noen som helst vits i spamme maldocs med de. Vipermonkey er et fint verktøy å teste for å sjekke om man enkelt kan få ut payload fra makroen.
 ![Vipermonkey siste versjon på en en kali-vm med 4Gb ram.](imgs/4_vmonkey_kali.png "Vipermonkey siste versjon på en en kali-vm med 4Gb ram")
 Kali linux med 4GB ram klarer ikke å tygge gjennom maldocen og feiler etter en god stund.

 ![Fedora VM med X RAM også med nyeste versjon vipermonkey](imgs/2020-04-21_21-00.png "Fedora VM med X RAM også med nyeste versjon vipermonkey")

 På en Fedora VM med 16 GB RAM fullfører Vipermonkey. Base64-strenger er den vi allerede har sett i manuell løsning, og jeg går derfor ikke videre her.
