Oppgavene for Initial Compromise er alle varianter av `Maldocs`.

Tiltenkt rekkefølge på oggavene er:

- [Registrert bibliotek](Registrert\ bibliotek/OPPGAVE.md)
- [Kraftskall](Kraftskall/OPPGAVE.md)
- [Trampet](Trampet/OPPGAVE.md)
- [En uheldig eggejakt](En\ uheldig\ eggejakt/OPPGAVE.md)
- [toteslegit](toteslegit/OPPGAVE.md)
