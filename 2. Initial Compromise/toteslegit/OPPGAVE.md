Denne gangen er det flere brukere som har motatt et Excel-ark som later til å være helt legit og inneholder et budsjett. Tittelen på filen sier iallefall at den er legit. Kan du sjekke om dette dokumentet er legit, eller om det inneholder gjemte EGG?

OBS: Samme advarsel (og passord) som forrige oppgave.

Lever `EGG{32-byte hex}`

FIL: [5_VeryLegitBudgetPlzEnableMacro.7z](5_VeryLegitBudgetPlzEnableMacro.7z)

[HINT](HINT.md)