Brukeren begynner å bli irritert. Det er mange e-poster med vedlegg, men disse kan ikke åpnes.

Det har kommet nok en e-post, og denne er videresendt til analysesentret. E-posten er sjekket og brukeren mener alt ser bra ut.

Er det trygt å åpne vedlegget?

OBS: Samme advarsel (og passord) som forrige oppgave.

Lever `EGG{32-byte hex}`

FIL: [3_Partial_Payment_5186.7z](3_Partial_Payment_5186.7z)

[HINT](HINT.md)