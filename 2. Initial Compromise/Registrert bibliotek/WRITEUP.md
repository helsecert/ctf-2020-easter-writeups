## Writeup: Registrert bibliotek

### Oppgavetekst og hint
![Dagens oppgaver er alle høyst reelle angrepsvektorer, og filene du skal analysere benytter de samme metodene og teknikkene som dagsaktuelle angrep, både målrettede og generelle. Hvis du klarer å løse tre eller flere av dagens fem oppgaver er du godt rustet til å analysere denne typen angrep fremover. En bruker har rapportert mottak av en e-post som han syntes så mistenkelig ut. E-posten hadde en word-fil vedlagt og vi lurer på om denne filen er clean, eller om den inneholder angrepskode. Analysesentret har ikke kapasitet til å se på denne saken. Kan du sjekke om det er noen råtne EGG i filen? En av våre beste kyllinger har skrevet litt bakgrunnsinfo om dette temaet: maldocs Han nevner også at det kan være lurt å lese seg litt opp her og her OBS: Filene er ZIPet og passordbeskyttet siden antivirus på din datamaskin kan bli sint. Vi anbefaler at du jobber med filen på en datamaskin uten AV. Det er ikke faktisk skadevare i noen av disse filene. Passord 7z: healthy_nuts Lever EGG{32-byte hex}](imgs/kraftskall_1.png "Dagens oppgaver er alle høyst reelle angrepsvektorer, og filene du skal analysere benytter de samme metodene og teknikkene som dagsaktuelle angrep, både målrettede og generelle. Hvis du klarer å løse tre eller flere av dagens fem oppgaver er du godt rustet til å analysere denne typen angrep fremover. En bruker har rapportert mottak av en e-post som han syntes så mistenkelig ut. E-posten hadde en word-fil vedlagt og vi lurer på om denne filen er clean, eller om den inneholder angrepskode. Analysesentret har ikke kapasitet til å se på denne saken. Kan du sjekke om det er noen råtne EGG i filen? En av våre beste kyllinger har skrevet litt bakgrunnsinfo om dette temaet: maldocs Han nevner også at det kan være lurt å lese seg litt opp her og her OBS: Filene er ZIPet og passordbeskyttet siden antivirus på din datamaskin kan bli sint. Vi anbefaler at du jobber med filen på en datamaskin uten AV. Det er ikke faktisk skadevare i noen av disse filene. Passord 7z: healthy_nuts Lever EGG{32-byte hex}")  
Etter to dager ble hint for registrert bibliotek lagt ut.  
![Hint: Er det noen snille strenger her?](imgs/kraftskall_2.png "Hint: Er det noen snille strenger her?")  

### Analytikerløsning
1. Kjører file for å sjekke filtype.
![Output av filkommando. Kjørt som intro for å sjekke hvilken dokumenttype det er snakk om](imgs/2020-04-16_20-39.png "Output av filkommando. Kjørt som intro for å sjekke hvilken dokumenttype det er snakk om")

 Vi ser at dette er et composite document, samt noe metadata om hvem som har laget det, editert det sist etc. Det meste av denne metadataen er sannsynligvis forfalsket om dette er et faktisk maldoc. (Spoiler alert: mesteparten er forfalsket)

2. Vi bruker deretter olevba fra [oletools](https://github.com/decalage2/oletools) for å sjekke etter macroer siden det har vært standard skadevarelevering i maldocs (som ikke er av typen RTF...) en god stund.
 ![Resultat av olevba -c for å dumpe bare makrokoden](imgs/Y2020-04-16_20-28.png "Resultat av olevba -c for å dumpe bare makrokoden")

  Vi ser at her fåt vi EGGet printet ut sammen med macroen fra olevba uten å kjøre noe ekstra jobb.

### Alternativ løsning
 Denne oppgaven er en ganske basic maldoc uten noen særlig form for obfuskering eller andre triks og vi kan enkelt hente ut flagget enten med strings, eller med en hex-editor.

#### Strings:
 Vi kjører en enkelt strings og grepper etter ´EGG´.  
 ![Resultat av strings mot word dokumentet](imgs/2020-04-16_20-29.png "Resultat av strings mot word dokumentet")

#### Hex editor:
 Vi kan også løse denne ved å åpne filen i en hex-editor som f.eks. bless.  
 ![Fil åpnet i bless og søkt etter EGG](imgs/2020-04-16_10-11.png "Fil åpnet i bless og søkt etter EGG")

