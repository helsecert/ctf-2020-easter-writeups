Dagens oppgaver er alle høyst reelle angrepsvektorer, og filene du skal analysere benytter de samme metodene og teknikkene som dagsaktuelle angrep, både målrettede og generelle. Hvis du klarer å løse tre eller flere av dagens fem oppgaver er du godt rustet til å analysere denne typen angrep fremover.

En bruker har rapportert mottak av en e-post som han syntes så mistenkelig ut. E-posten hadde en word-fil vedlagt og vi lurer på om denne filen er clean, eller om den inneholder angrepskode.

Analysesentret har ikke kapasitet til å se på denne saken. Kan du sjekke om det er noen råtne EGG i filen?

En av våre beste kyllinger har skrevet litt bakgrunnsinfo om dette temaet: maldocs Han nevner også at det kan være lurt å lese seg litt opp her og her

OBS: Filene er ZIPet og passordbeskyttet siden antivirus på din datamaskin kan bli sint. Vi anbefaler at du jobber med filen på en datamaskin uten AV. Det er ikke faktisk skadevare i noen av disse filene.

Passord 7z: `healthy_nuts`

Lever `EGG{32-byte hex}`

FIL: [1_Overdue_Ticket_4825.7z](1_Overdue_Ticket_4825.7z)

[HINT](HINT.md)