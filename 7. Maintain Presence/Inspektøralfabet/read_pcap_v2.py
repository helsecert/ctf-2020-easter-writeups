from scapy.all import *
packets = rdpcap("c2_trafikk_v2.pcap")
speed, start = 1, packets[0].time
buf = ['0' for i in range(round(packets[-1].time-start)+1)]
for i in range(len(packets)):
    buf[int(round(packets[i].time - start)/speed)] = '1'
print("".join(buf))

# buf="1010101000111011101110001"

DIH = '1'
DAH = '111'
INTRA_CHARACTER_GAP = "0"         # between the dots and dashes within a character
SHORT_GAP = "000"                 # between letters
MEDIUM_GAP = "0000000"            # between words
print(("".join(buf)).replace(MEDIUM_GAP, "   ").replace(SHORT_GAP, " ").replace(
    DAH, '-').replace(DIH, '.').replace(INTRA_CHARACTER_GAP, ""))
