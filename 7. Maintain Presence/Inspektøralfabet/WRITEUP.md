Oppgaveteksten er lang og har mye informasjon, med en vedlagt PCAP. Oppgavens tittel viser til `Inspektør Morse` (britisk tv-serie) og `morse` som et alfabet. Det er viktig å forstå innholdet i teksten og lese ut hintene. Her siktes det til `morse` med `OOK` som er både `på og av`, og `intervaller` med `sekunder`. `OOK` kan virke som en skrivefeil i teksten, og det var bevisst slik at man skulle tolke dette som et hint.

Hvis man googler `morse OOK` finner vi [On-off keying](https://en.wikipedia.org/wiki/On%E2%80%93off_keying "on-off keying") som er en metode for å overføre binær 1 med et signal, mens fravær av signal representerer binær 0. 

Hvis vi åpner PCAP filen i Wireshark oppdager vi raskt at innholdet stemmer med oppgaven `Kryss eller samtale`. For de som laget seg en dekoder for "kryss eller samtale"-oppgaven så kan denne gjenbrukes her. Det meste vi får ut av meldinger er om `RØD SILD`, som er et velkjent begrep i CTFer om at man ikke finner et flagg (falsk positiv). Altså, du finner ikke EGGet i meldingene i HTTP pakkene. Det er dog andre meldinger foruten rød sild: (bruker vedlagt [script som ble laget for Kryss eller Samtale](read_pcap.py "lite script"))
```bash
$ python read_pcap.py | grep -v "RØD SILD"
en telegraf forstår dette
egget er sendt med klokka
hvis du bruker mer tid finner du egget
timing er viktig, både signal på og signal av
fravær av kommunikasjon er også informasjon
med tid kan vi kommunisere
tid er informasjon
```

Her gjentas en del av hintene fra oppgaveteksten, både dette med `morse` (telegraf), at tid er viktig (sekunder/intervaller) og `signal på og signal av`.

Hvis vi lister ut pakkene i PCAP filen med `tcpdump` ser vi at tiden for hver pakke har intervaller som er ca 1 sekund med noe desimal-presisjon. Dog, denne er alltid mindre enn <100 ms og vi ser derfor bort fra desimal-presisjonen. Vi kan dumpe tiden per pakke, i hele sekunder (de 14 første pakkene)
```bash
$ tcpdump -nn -r c2_trafikk_v2.pcap -tt | awk -F'.' '{ print $1}' | head -n14

1585770001
1585770003
1585770005
1585770007
1585770011
1585770012
1585770013
1585770015
1585770016
1585770017
1585770019
1585770020
1585770021
1585770025
```

Oppgaven handler om at det er brukt `morse` og `OOK` signalering. På wiki for [Morse Code](https://en.wikipedia.org/wiki/Morse_code#Representation,_timing,_and_speeds "wiki") finner vi en del detaljer om hvordan dette kan foregå.

En pakke (HTTP) er et signal og derfor binær 1, mens fravær av en pakke er en binær 0. Tidsintervallet er 1 sekund. Vi kan da manuelt dekode de første 14 pakkene for å vise hvordan flyten vil gå. Vi setter inn 0 for alle hele sekunder det ikke er pakker, og 1 for alle sekunder som har en pakke:
```
1585770001      1
                0
1585770003      1
                0
1585770005      1
                0
1585770007      1
                0
                0
                0
1585770011      1       
1585770012      1
1585770013      1
                0
1585770015      1
1585770016      1
1585770017      1
                0
1585770019      1
1585770020      1
1585770021      1
                0
                0
                0

... osv
```

Vi får ut `101010100011101110111000`. Ved å se på wiki siden for Morse finner vi ut hvordan vi kan dekode binær-strømmen:
```
    short mark, dot or "dit" (▄▄▄▄): 1
    longer mark, dash or "dah" (▄▄▄▄▄▄): 111
    intra-character gap (between the dots and dashes within a character): 0
    short gap (between letters): 000
    medium gap (between words): 0000000
```

Et kjapt python script senere viser oss hva denne binær-strømmen vi fant betyr:
```python
buf = "1010101000111011101110001"

DIH = '1'
DAH = '111'
INTRA_CHARACTER_GAP = "0"         # between the dots and dashes within a character
SHORT_GAP = "000"                 # between letters
MEDIUM_GAP = "0000000"            # between words

print(("".join(buf)).replace(MEDIUM_GAP, "   ").replace(SHORT_GAP, " ").replace(
    DAH, '-').replace(DIH, '.').replace(INTRA_CHARACTER_GAP, ""))
```

Vi får ut `.... ---` altså `HO`. Å gjøre dette manuelt tar for lang tid. Vi lager nok et [lite python script](read_pcap_v2.py "read morse") som automatisk setter inn `0` for fravær av pakke og `1` på de sekundene vi ser en pakke. Vi får da ut en binærstrøm, som vi dekoder til morse:
```
1010101000111011101110001010100011100011101000101110001110111000100000001010100010111010001010101110000000111010100011101110111000111000000010101010001000111011100011101110001000101110101000101000111011101000111011101110111011100010101110111011100000001110101000111011101110001110000000111010111010001110001010111010000000101011100010100011101010000000111011101110111011100000001010001110101000101110101000100000001010101110111000101110111011101110001110101010100010101110111011100010101000000000001011100011101011101000111010111010001000101110111010001110000000101010001011101000101010111000000011101010001110111011100011100000001010101000100011101110001110111000100010111010100010100011101110100011101110111011101110001010111011101110000000111010100011101110111000111000000011101011101000111000101011101000000010111010100010100010101000111000000000001010101000111011101110001010100011100011101000101110001110111000100000001010100010111010001010101110000000111010100011101110111000111000000010101010001000111011100011101110001000101110101000101000111011101000111011101110111011100010101110111011100000001110101000111011101110001110000000111010111010001110001010111010000000100011101110100011101110100000001110101000111011101110001110000000111000111010101110001110000000000010111000111010111010001110101110100010001011101110100011100000001010100010111010001010101110000000111010100011101110111000111000000010101010001000111011100011101110001000101110101000101000111011101000111011101110111011100010101110111011100000001110101000111011101110001110000000111010111010001110001010111010000000111010111010001011100011100000001000111011101000111011101000000011101010001110111011100011100000001110001110101011100011100000000000101010100011101110111000101010001110001110100010111000111011100010000000101010001011101000101010111000000011101010001110111011100011100000001010101000100011101110001110111000100010111010100010100011101110100011101110111011101110001010111011101110000000111010100011101110111000111000000011101011101000111000101011101000000010001110111010001110111010000000111010111000101110100011101110111011101110001011101010001011101010001110101110111010000000111011101110111010001010111010001110101010001110101110100011101010100010101011101110001110111010101000111011101110111010001110111011101110100011101010100011101010101000101110001010101110111000101110001110111011101110111000111010100010101110100011101110111011101000111010101010001110101010001010101010001110101010001010111011101110001110111011101010001011100010001110111011101110100010001011100010101110111011100010101010100011101010101000000011101011100010111010001110111011101110111000101110101000101110101000111010111011101011100000000000101110001110101110100011101011101000100010111011101000111000000010101000101110100010101011100000001110101000111011101110001110000000101010100010001110111000111011100010001011101010001010001110111010001110111011101110111000101011101110111000000011101010001110111011100011100000001110101110100011100010101110100000001010100010001110001010001110101000101110101000100000001110111010101000101110111011101110001110101010100010101

.... --- ... - -. .- -- .   ... .-. ...-   -.. --- -   .... . -- -- . .-.. .. --. ----- ..---   -.. --- -   -.-. - ..-.   ..- .. -..   -----   .. -.. .-.. .   ...-- .---- -.... ..--- ...    .- -.-. -.-. . .--. -   ... .-. ...-   -.. --- -
   .... . -- -- . .-.. .. --. ----- ..---   -.. --- -   -.-. - ..-.   .-.. .. ... -    .... --- ... - -. .- -- .   ... .-. ...-   -.. --- -   .... . -- -- . .-.. .. --. ----- ..---   -.. --- -   -.-. - ..-.   . --. --.   -.. --- -   - -..
- -    .- -.-. -.-. . .--. -   ... .-. ...-   -.. --- -   .... . -- -- . .-.. .. --. ----- ..---   -.. --- -   -.-. - ..-.   -.-. .- -   . --. --.   -.. --- -   - -..- -    .... --- ... - -. .- -- .   ... .-. ...-   -.. --- -   .... . -- 
-- . .-.. .. --. ----- ..---   -.. --- -   -.-. - ..-.   . --. --.   -.- .-. ----- .-.. .-.. -.--.   ----. ..-. -... -.-. -... ...-- --... ----. ----. -... -.... .- ...-- .- ----- -.. ..-. ----. -.... -... ..... -... ..--- ---.. .- . ----
. . .- ..--- ..... -....   -.- .-. ----- .-.. .-.. -.--.-    .- -.-. -.-. . .--. -   ... .-. ...-   -.. --- -   .... . -- -- . .-.. .. --. ----- ..---   -.. --- -   -.-. - ..-.   ... . - .. -.. .-.. .   --... .---- -.... ...

```

Morse-koden kan vi oversette online. En god dekoder vil gi dette:
```
HOSTNAME  SRV  DOT  HEMMELIG02  DOT  CTF  UID  0  IDLE  3162S   ACCEPT  SRV  DOT  HEMMELIG02  DOT  CTF  LIST   HOSTNAME  SRV  DOT  HEMMELIG02  DOT  CTF  EGG  DOT  TXT   ACCEPT  SRV  DOT  HEMMELIG02  DOT  CTF  CAT  EGG  DOT  TXT   HOSTNAME  SRV  DOT  HEMMELIG02  DOT  CTF  EGG  KR0LL(  9FBCB3799B6A3A0DF96B5B28AE9EA256  KR0LL)   ACCEPT  SRV  DOT  HEMMELIG02  DOT  CTF  SETIDLE  716S
```

I pakkestrømmen går det trafikk i to retninger, `klient (k)->server (s)` og `server->klient`. Hvis vi splitter opp morse-dekodingen får vi lest trafikken slik den gikk hver vei.
```
k->s: HOSTNAME  SRV  DOT  HEMMELIG02  DOT  CTF  UID  0  IDLE  3162S
s->k: ACCEPT  SRV  DOT  HEMMELIG02  DOT  CTF  LIST

k->s: HOSTNAME  SRV  DOT  HEMMELIG02  DOT  CTF  EGG  DOT  TXT
s->k: ACCEPT  SRV  DOT  HEMMELIG02  DOT  CTF  CAT  EGG  DOT  TXT

k->s: HOSTNAME  SRV  DOT  HEMMELIG02  DOT  CTF  EGG  KR0LL(  9FBCB3799B6A3A0DF96B5B28AE9EA256  KR0LL)
s->k ACCEPT  SRV  DOT  HEMMELIG02  DOT  CTF  SETIDLE  716S
```

De som fikk ut hele innholdet i `Kryss eller samtale` vil gjenkjenne dette.

Flagg (case-insensitive, siden morse ikke har upper/lower case):

`EGG{9FBCB3799B6A3A0DF96B5B28AE9EA256}`

`EGG{9fbcb3799b6a3a0df96b5b28ae9ea256}`
