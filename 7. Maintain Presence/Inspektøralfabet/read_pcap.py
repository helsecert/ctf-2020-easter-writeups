from scapy.all import *
from scapy.layers.http import HTTPRequest, HTTPResponse
import base64

filename = "c2_trafikk_v2.pcap"


def xor_decode(m, k):
    r = bytes([ch ^ k for ch in m])
    return r.decode("utf-8")


packets = rdpcap(filename)
for i in range(len(packets)):
    pkt = packets[i]

    # Finner XOR key per pakke.
    xorkey = 0
    if pkt.haslayer(HTTPRequest):
        if b'X-Secret' in pkt[HTTPRequest].Unknown_Headers:
            xorkey = int(pkt[HTTPRequest].Unknown_Headers[b'X-Secret'])
    if pkt.haslayer(HTTPResponse):
        if b'X-Secret' in pkt[HTTPResponse].Unknown_Headers:
            xorkey = int(pkt[HTTPResponse].Unknown_Headers[b'X-Secret'])

    if xorkey != 0:
        if pkt.haslayer(Raw):
            data = (pkt[Raw].load).decode("utf-8").strip()

            b = base64.b64decode(data)
            dekodet = xor_decode(b, xorkey)
#            print(f"> xorkey:{xorkey}  data:{data}")
            print(dekodet)
