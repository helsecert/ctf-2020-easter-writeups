Etter at påskekyllingene har vært på cyberopplæring har antall saker i analysesentret økt betraktelig. De har lært at angriper vil forsøke å opprette og beholde kommando og kontroll (c2) på datamaskinene som er kompromittert. Ofte kan en angriper ta seg god tid til å overføre data, for å unngå å bli oppdaget.

Tidligere samme dag har en erfaren påskekylling oppdaget mer c2-trafikk. Etter mange timer med graving og analyser begynner tankene hans å vandre tilbake til tidlig barndom. Hans bestefar hadde vært i militæret som telegraf. Mens de så britisk krim på TV øvde de seg på å knekke både korte og lange meldinger. Timingen var viktig, både når signalet skal være på og når det skal være av. Det var så spennende, og tenk at dette var jo mye brukt lenge før datamaskinene.

Litt senere på dagen ser han tilfeldigvis på klokka. Han studerer intervallene på sekundviseren nøye. Da oppdager han at bussen snart går, OOK sier han, låser pc'en og løper ut døra.

Kan du se om det er overført noe viktig i c2-trafikken?

Lever `EGG{32-byte hex}`

Fil: [c2_trafikk_v2.pcap.7z](c2_trafikk_v2.pcap.7z)

[HINT](HINT.md)