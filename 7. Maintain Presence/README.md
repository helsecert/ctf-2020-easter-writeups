Oppgavene for `Maintain Presence` er tiltenkt å gjøres i følgende rekkefølge:

- [Kryss eller samtale](Kryss\ eller\ samtale/OPPGAVE.md)
- [Påskeegg i solnedgang](Påskeegg\ i\ solnedgang/OPPGAVE.md)
- [Inspektøralfabet](Inspektøralfabet/OPPGAVE.md)