Oppgave
=======

Krise! Angriper har fått et godt fotfeste i organisasjonen! Påskekyllingene er opptatt med å tømme varm sjokolade på serverne for å holde uvedkommende ute, men vi tror vi har oppdaget noe som må sjekkes nærmere.

Vi mistenker at angriper har brukt et nettskall til å kjøre litt hemmelig kode som skal sikre dem et EGG.

Klarer du å finne egget?

Lever `EGG{32-byte hex}`

Fil: [task.zip](task.zip)

[HINT](HINT.md)