To påskekyllinger er sikre på at de har funnet kommando & kontroll (c2)-trafikk, altså at en angriper kan sitte en plass langt ute på internett og styre en datamaskin hos Påskeharen.

I samarbeid har de greid å knekke den første meldingen ved å bruke brute force. De fant at nøkkelen `96` dekoder `CA8TFA4BDQVaQDMyNk0oJS0tJSwpJ1BSTiM0JmoVCQRaQFBqCQQMBVpAU1FWUhNq` til klartekst:
<pre>
hostname: SRV-HEMMELIG02.CTF
uid: 0
idle: 3162s
</pre></br>

Samme nøkkel fungerer ikke på de resterende meldingene. 

Kan du dekryptere resten av trafikken?

Lever `EGG{32-byte hex}`

Fil: [c2_trafikk.pcap.7z](c2_trafikk.pcap.7z)

[HINT](HINT.md)