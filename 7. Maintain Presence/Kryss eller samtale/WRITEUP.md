Oppgaven gir oss en PCAP og et eksempel. Ved å brute-force `CA8TFA4BDQVaQDMyNk0oJS0tJSwpJ1BSTiM0JmoVCQRaQFBqCQQMBVpAU1FWUhNq` med nøkkelen `96` får man ut klartekst. Oppgavens tittel er et ordspill for `XOR` (som vi snart vil oppdage).

Vi undersøker PCAP filen i Wireshark og ser trafikk mellom en klient og en server, og at meldingene ikke går i klartekst. I Wireshark velger vi "Follow HTTP stream" og får ut trafikken:

```
POST /main.php/control HTTP/1.1
Host: go0rx.ctf
User-Agent: c2.exe
X-Secret: 124
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: base64
Content-Length: 64

FBMPCBIdERlGXC8uKlE0OTExOTA1O0xOUj8oOnYJFRhGXEx2FRgQGUZcT01KTg92

HTTP/1.1 202 Accepted
Server: Meldingsmottak/0.1.2 (Kali)
X-Secret: 40
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: base64
Content-Length: 56

SUtLTVhcEgh7en4FYG1lZW1kYW8YGgZrfG4iS0dFRUlGTBIIREFbXCI=

... osv
```

I HTTP headers ser vi `Content-Transfer-Encoding: base64`. Eksemplet i oppgaven er altså base64. Vi åpner [cyberchef](https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)XOR(%7B'option':'Decimal','string':'96'%7D,'Standard',false)&input=Q0E4VEZBNEJEUVZhUURNeU5rMG9KUzB0SlN3cEoxQlNUaU0wSm1vVkNRUmFRRkJxQ1FRTUJWcEFVMUZXVWhOcQ "cyberchef") og velger `From Base64` og `XOR` med nøkkel `96` (decimal) og limer inn strengen fra eksemplet. Da får vi ut klarteksten. Hvis vi forsøker en annen melding fra trafikken med samme xor-key blir det bare søppel som output. Vi må riktig `xor-key` for hver melding.

Vi oppdager at HTTP header `X-Secret` varierer for hver melding, med et tall. Dette er nok xor-key. Vi forsøker enda en runde i cyberchef hvor vi parvis bruker `X-Secret` som xor-key for hver base64-enkodet melding. Da får vi dekodet hele c2-trafikken.

Vi kan nå manuelt dekode hver melding, eller skrive et raskt [lite script](read_pcap.py "lite script") som gjør jobben. Vi får ut trafikken og finnet et EGG!

```
> xorkey:124  data:FBMPCBIdERlGXC8uKlE0OTExOTA1O0xOUj8oOnYJFRhGXEx2FRgQGUZcT01KTg92
hostname: SRV-HEMMELIG02.CTF
uid: 0
idle: 3162s

> xorkey:40  data:SUtLTVhcEgh7en4FYG1lZW1kYW8YGgZrfG4iS0dFRUlGTBIIREFbXCI=
accept: SRV-HEMMELIG02.CTF
command: list

> xorkey:21  data:fXpmYXt0eHAvNUZHQzhdUFhYUFlcUiUnO1ZBUx9ncGZgeWEvH3BycjthbWEf
hostname: SRV-HEMMELIG02.CTF
result:
egg.txt

> xorkey:123  data:GhgYHgsPQVsoKS1WMz42Nj43MjxLSVU4Lz1xGBQWFhoVH0FbGBoPcRoJHA4WHhUPCEFbHhwcVQ8DD3E=
accept: SRV-HEMMELIG02.CTF
command: cat
arguments: egg.txt

> xorkey:49  data:WV5CRV9QXFQLEWJjZxx5dHx8dH14dgEDH3JldztDVEJEXUULO3R2dkpQBAQBBAYCB1QFBwQIBAgIBAFUBgQEAlVUA1AHAgcGAUw7
hostname: SRV-HEMMELIG02.CTF
result:
EGG{a5505736e465959950e7553de2a63670}

> xorkey:99  data:AgAABhMXWUMwMTVOKyYuLiYvKiRTUU0gNyVpAAwODgINB1lDEAYXTgoHDwZpAhEEFg4GDRcQWUNUUlUQaQ==
accept: SRV-HEMMELIG02.CTF
command: set-idle
arguments: 716s

> xorkey:40  data:QEdbXEZJRU0SCHt6fgVgbWVlbWRhbxgaBmt8biJaTVtdRFwSCElLQyI=
hostname: SRV-HEMMELIG02.CTF
result: ack
```

Flagg: `EGG{a5505736e465959950e7553de2a63670}`


