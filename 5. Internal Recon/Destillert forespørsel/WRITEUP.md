Oppgaven viser en logg fra en LDAP server og man skal finne et brukernavn og passord som er kompromittert.  Oppgaven er gjort noe vanskeligere ved at man må håndtere 67MB med logg. Dette er en normal situasjon for en sikkerhetsanalytiker; at dataene man skal jobbe på er uoversiktlig og ofte store mengder.

Det er hint bådt i tittel og i tekst hvor man skal `filtrere` loggen. Hvis man har erfaring med LDAP fra før så burde også dette være kjent. 

Hvis man søker etter `filter=(` i loggen så vil man finne hva angriper har gjort av LDAP spørringer.

```bash
$ grep "filter=\"(" ldap.log   

5e7481c0 conn=1029 op=1 SRCH base="" scope=0 deref=0 filter="(objectClass=*)"
5e7481c0 conn=1031 op=1 SRCH base="ou=groups,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(objectClass=*)"
5e7481c0 conn=1032 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(objectClass=*)"
5e7481c2 conn=1033 op=1 SRCH base="ou=groups,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(objectClass=*)"
5e7481c4 conn=1034 op=1 SRCH base="cn=sysadmin,ou=groups,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(objectClass=*)"
5e7481c7 conn=1035 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(gidNumber=1100)"
5e7481cb conn=1036 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(gidNumber=1100)"
5e7481ce conn=1037 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(cn=eryn8)"
5e7481d1 conn=1038 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(cn=chad42)"
5e7481d4 conn=1039 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(cn=lena53)"
5e7481d6 conn=1040 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(cn=waly57)"
5e7481da conn=1041 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(cn=simone9)"
5e7481dd conn=1042 op=1 SRCH base="ou=users,dc=pasientjournal,dc=ctf" scope=2 deref=0 filter="(cn=sysadmin)"
```

Angriper gjør spørringer etter grupper og brukere, og viser ekstra interesse for en spesifikk gruppe `gidNumber=1100` (sysadmin). Videre spørres det etter brukere i denne gruppen med `cn` (common name). Angriper får ut `userPassword` fra hver bruker, som et hash. Google sier oss at OpenLDAP følger RFC2307 og vi finner en liste med supporterte hashede passord på formatet `{<navn på hash-algo>}`. Vi søker etter dette i loggen:
```bash
$ grep -Po "{[^{]+}" ldap.log | sort | uniq -c
     13 {M}}
      8 {mm}
      6 {SHA}
   7022 {SSHA}
```

De første to er bare søppel fra loggen. `SHA` og `SSHA` er SHA1 med og uten salt/seed. Årsaken til duplikater er at ting er logget flere ganger. Som beskrevet for writeup `128-bit hash value` så vil det være mer krevende å knekke/finne passord som er saltet, derfor konsentrerer vi oss om det som er usaltet. Passord-hashet er base64 enkodet, og vi gjør det om til 20byte hexdigest:
```bash
$ grep "{SHA}" ldap.log -A2
  00e0:  77 6f 72 64 31 23 04 21  7b 53 48 41 7d 4e 52 52   word1#.!{SHA}NRR  
  00f0:  55 4a 74 30 66 30 44 47  72 35 66 39 49 73 6d 2b   UJt0f0DGr5f9Ism+  
  0100:  54 42 42 71 49 4d 4d 34  3d 30 1b 04 10 73 68 61   TBBqIMM4=0...sha  

$ echo -n "NRRUJt0f0DGr5f9Ism+TBBqIMM4=" | base64 -d | xxd -p
35145426dd1fd031abe5ff48b26f93041a8830ce
```

Nå kan vi knekke dette passordet. Vi vet ikke lengden eller kompleksiteten på passordet, og førsøker derfor først å søke opp det i en online crack-database, som f.eks. crackstation.net. Her finner vi passordet `easterbunny`.
```
Hash                                        Type    Result
35145426dd1fd031abe5ff48b26f93041a8830ce    sha1    easterbunny
```

Nå mangler vi bare å finne hvilken bruker som hadde dette passordet. Vi finner brukeren `cn=lena53,ou=users,dc=pasientjournal,dc=ctf"` og brukernavnet er `lena53`.

Svaret på oppgaven er `lena53:easterbunny`
```bash
$ echo -n "lena53:easterbunny" | md5sum 
386ea985fea4ea456ee23d9490fee748  -
```

Flagg: `EGG{386ea985fea4ea456ee23d9490fee748}`
