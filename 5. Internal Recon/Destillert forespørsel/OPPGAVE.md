Nå vet vi at angriper aktivt har søkt etter LDAP-tjenere internt. LDAP brukes i større virksomheter for å sentralisere brukerinformasjon, som brukernavn og passord. Angrep mot LDAP kan være en forberedelse på sideveis bevegelse. Ved å finne brukernavn og passord, gjerne med store rettigheter som systemadministrator, kan denne informasjonen være nyttig senere.

En ivrig cyberoperatør i analysesentret har snappet opp en melding på internett. I et bilde av påskeharen lå det en skjult melding, som ble lagt ut noen timer etter at LDAP-søket var ferdig. Filtrert ut fra bildet fant man dette:

`w3 0wn p445k3h4r3n5 1nfr4! 345yp345y! u53r + p455 pwnd! l0lz`

Vi antar dette kommer fra angriper. Kan du filtrere loggen for å se hvilket brukernavn og passord som er kompromittert?

Lever `EGG{md5sum(<brukernavn>:<passord>)}`

Fil: [ldap.log.7z](ldap.log.7z)

[HINT](HINT.md)