I brannmurloggen vises et internt scan.

Oppgaven løses ved å forstå hvilke IPer som gir svar ved å se på TCP flaggene. Disse ligger sist i logglinjene som `SYN, ACK, RST, FIN`.

I oppgavene for dag1 så får man ut `netflow`, dette er (ofte) metadata om en bi-direksjonell kobling. Da får man korrelert flaggene i begge retninger. I loggen i denne oppgaven ser man hver pakke som sendes.

For å løse oppgaven kan man f.eks. bygge seg en egen netflow-aggregator (se vedlagt fil). Den lager nesten det samme innholdet som vi brukte i oppgavene for dag1. Når scriptet kjøres så ser vi at det er en del koblinger (ip-ip) som kun har SYN eller ACK flagget satt. Disse kan vi filtrere ut, siden det er hoster som ikke har gitt svar. Scriptet viser en netflow to ganger, ut fra hvem som har initiert koblingen. Vi ser at det går mot port 389, og vi kan derfor filtrere på trafikken som er initiert fra klienten:

```bash
$ python read.py | grep -v -P " (S_|A_)$" | grep ":389 "

192.168.109.62:49686-172.23.0.1:389 SAR_AS
192.168.109.62:49972-172.23.0.1:389 SAR_AS
192.168.109.62:51926-172.23.0.1:389 SAR_AS
192.168.109.62:40304-172.23.0.1:389 A_R
```

Riktig svar er `172.23.0.1:389`. Port 389 er ldap (google it eller sjekk `/etc/services`)

```bash
$ echo -n "ldap://172.23.0.1" | md5sum 
8b8a5b7c4eac59bc1fd1088ca1a532d8  -
```

Flagg: `EGG{8b8a5b7c4eac59bc1fd1088ca1a532d8}`