Angriper har gjort et horisontalt skann etter en tjeneste. Kan du finne ut hvilket navn det er på protokollen som kartlegges, og hvilken IP-adresse angriper fant?

Eksempel: Hvis du finner port 80 og IP 10.0.0.1 så vil svaret være `http://10.0.0.1`. Protokoll er små bokstaver.

Lever `EGG{md5sum(<protokoll>://<ip-adresse>)}`

Fil: [brannmurlogg.txt.7z](brannmurlogg.txt.7z)

[HINT](HINT.md)