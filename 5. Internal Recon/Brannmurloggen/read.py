import re

flgs = {
    "SYN": "S",
    "ACK": "A",
    "RST": "R",
    "FIN": "F",
}

p = 'SRC=([^ ]+) DST=([^ ]+).*SPT=([^ ]+) DPT=([^ ]+).*RES=0x00 (.+) URGP=0'
m = {}
filename = "brannmurlogg.txt"
with open(filename, 'rb') as f:
    for line in f:
        line = line.lstrip().rstrip().decode("utf-8")

        r = re.search(p, line)
        if r == None:
            continue

        src = "%s:%s" % (r.group(1), r.group(3))
        dst = "%s:%s" % (r.group(2), r.group(4))
        s = f"{src}-{dst}"
        if not s in m.keys():
            m[s] = {}
        for f in r.group(5).split(" "):
            m[s][flgs[f]] = 1

for k in m:
    z = k.split("-")
    src = z[0]
    dst = z[1]
    s = f"{dst}-{src}"
    df = ""
    if s in m.keys():
        df = "".join(m[s].keys())
    sf = "".join(m[k].keys())
    print(k, "%s_%s" % (sf, df))
