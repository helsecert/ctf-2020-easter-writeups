Flere eksperter i analysesentret tror nå at angriper vil forsøke intern rekognosering. Dette er et søk etter blant annet brukernavn, passord, sertifikater eller andre ressurser som IP-adresser og domenenavn. Dette kan brukes senere, og man er spesielt ute etter sentral lagring av brukernavn og passord som f. eks. tilgangstjenere.

Tidligere i dag har angriper vist interesse for en intern nettside som bruker TLS. Sertifikatet lekker informasjon om andre interne nettsider. 

Kan du finne vertsnavnet (domenenavn) for den sikre nettsiden?

Lever `EGG{md5sum av <vertsnavnet>}`

Fil: [server.crt](server.crt)

[HINT](HINT.md)