Sertifikater kan ha Subject Alternative Name (SAN) hvor man tillegger ekstra informasjon som e-post og DNS navn.

I oppgaven er det et sertifikat som har ganske mange, og det gjelder å finne en som er "sikker". Det er mulig å løse dette ved å bare se på innholdet, men kanskje det er greit å kjøre noen kommandoer i kommando-linjen:

```bash
$ openssl x509 -in server.crt -text -noout -certopt no_subject,no_header,no_version,no_serial,no_signame,no_validity,no_issuer,no_pubkey,no_sigdump,no_aux | grep ":sikker" | tr ', ' '\n' | grep -v "^$" | cut -d":" -f2 | sort | uniq -c

    274 ganske-kjedelig-server.ctf
    252 ganske-normal-server.ctf
    230 ganske-uvanlig-server.ctf
    282 ganske-vanlig-server.ctf
    228 kjedelig-server.ctf
    252 normalt-usikker-server.ctf
    254 passe-kjedelig-server.ctf
    233 passe-normal-server.ctf
    280 passe-vanlig-server.ctf
      1 sikker-server.ctf
    259 temmelig-normal-server.ctf
    278 unormalt-kjedelig-server.ctf
    244 unormalt-usikker-server.ctf
    275 usikker-server.ctf
    253 uvanlig-server.ctf
    251 uvanlig-usikker-server.ctf
    250 vanlig-server.ctf

echo -n "sikker-server.ctf" | md5sum
623c6ab670dff4aaf21de89ca86de1ab  -
```

Flagg: `EGG{623c6ab670dff4aaf21de89ca86de1ab}`
