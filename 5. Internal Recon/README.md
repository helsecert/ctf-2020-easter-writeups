Oppgavene for `Internal Recon` har følgende tiltenkte rekkefølge:

- [En sikrere nettside](En\ sikrere\ nettside/OPPGAVE.md)
- [Brannmurloggen](Brannmurloggen/OPPGAVE.md)
- [Destillert forespørsel](Destillert\ forespørsel/OPPGAVE.md)

