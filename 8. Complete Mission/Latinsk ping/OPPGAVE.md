En påskekylling med stor interesse for latin har plukket opp en serie med ICMP-pakker fra nettverket. Det var noe i innholdet som trigget han. 

Først etterpå oppdaget han at dette kanskje var angriperen som testet om det var mulig å eksfiltrere data ut, over ICMP!?

Kan du sette sammen innholdet slik at du får ut dokumentet som er overført?

Lever `EGG{md5sum av dokumentet}`

Fil: [icmp.pcap.7z](icmp.pcap.7z)

[HINT](HINT.md)