I oppgaven er det en PCAP fil som viser ICMP trafikk med innhold. En angriper har her overført et dokument ved å fragmentere det, og legge bitene inn i payload på mange ICMP pakker.

ICMP er oftest forbundet med kommandoen `ping`, som er et hjelpeprogram for å sende et "ekko" til en datamaskin for å både beregne hvor lang tid det tar før man får svar, og om man i det hele tatt får et svar.

Oppgaven viser at det er mulig å overføre informasjon ut av et nettverk hvis ICMP er åpent. Har du en datamaskin på jobb som kan pinge f.eks. nrk.no så kan du overføre informasjon ut av nettverket. Dette gjør at en angriper kan bruke ICMP som en metode for å fullføre oppdraget ved å få informasjon ut og hjem til seg selv.

For å løse oppgaven må vi sette sammen bitene fra ICMP payload og gjenskape dokumentet. Merk at siste pakke har en 4 byte NULL (0x00) padding som må bort før man kjører md5sum av dokumentet. Et [lite python script](read_icmp.py "read icmp") gjør jobben:

```bash
$ python read_icmp.py > dok.txt  
$ md5sum dok.txt 
63e45b1cfbf1033f26d257f5545f8b9b  dok.txt

```

Flagg: `EGG{63e45b1cfbf1033f26d257f5545f8b9b}`