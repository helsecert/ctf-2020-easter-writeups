Dette var nok den oppgaven med flest layers; det var mye informasjon i oppgaveteksten, en powershell-kommando, PCAP med dns-trafikk, AES dekryptering, md5 brute-force kombinert med linked-lists og en haug med eval's. Dette gjorde kanskje at mange greide AES delen men ikke de siste bitene.

Først, powershell kommandoen. Den kan base64 dekodes og vi får ut en nøkkel (key) som også er base64 enkodet, et hint om `base58` encoding med `encoding 58` samt at det er AES. Tittelen i oppgaven gir vel liten tvil om det.
```
$ echo -n "cwBsAGUAZQBwACAAMwAwADAACgAkAGEAPQAiAHIAaQBqACIACgAkAHAAdABoAD0AJABMAG8AYwBhAGwAOgBIAE8ATQBFACsAIgBcAEEAcABwAEQAYQB0AGEAXABMAG8AYwBhAGwAXAAiAAoAZABpAHIAIAAkAHAAdABoAAoAJABlAD0AIgBuAGQAYQAiAAoAJABrAGUAeQA9AFsAUwB5AHMAdABlAG0ALgBUAGUAeAB0AC4ARQBuAGMAbwBkAGkAbgBnAF0AOgA6AFUAbgBpAGMAbwBkAGUALgBHAGUAdABTAHQAcgBpAG4AZwAoAFsAUwB5AHMAdABlAG0ALgBDAG8AbgB2AGUAcgB0AF0AOgA6AEYAcgBvAG0AQgBhAHMAZQA2ADQAUwB0AHIAaQBuAGcAKAAnAFUAMwBWAHcAWgBYAEoAVwBhAFcANQAwAFoAWABJAHkATQBEAEkAdwBJAFEAbwA9ACcAKQApAAoAJABzAD0AIgBlAGwAIgAKAFMAdABhAHIAdAAtAFAAcgBvAGMAZQBzAHMAIABlAHYAaQBsAF8AcwB2AGMAaABvAHMAdAAuAGUAeABlACAALQBlAG4AYwBvAGQAaQBuAGcAIAA1ADgAIAAtAGsAZQB5ACAAJABrAGUAeQAgAC0AYwByAHkAcAB0AG8AIAAkAGEAJABlACQAcwA=" | base64 -d

sleep 300
$a="rij"
$pth=$Local:HOME+"\AppData\Local\"
dir $pth
$e="nda"
$key=[System.Text.Encoding]::Unicode.GetString([System.Convert]::FromBase64String('U3VwZXJWaW50ZXIyMDIwIQo='))
$s="el"
Start-Process evil_svchost.exe -encoding 58 -key $key -crypto $a$e$s

$ echo -n "U3VwZXJWaW50ZXIyMDIwIQo=" | base64 -d
SuperVinter2020!
```

PCAP filen inneholder DNS-forespørsler og det ligger data i spørringene med følgende format:
```
  <data>.<session-id>.<domene>
```

Session-id er en random GUID, og vi ser at den er lik for de første 3 DNS requests. Vi kan derfor anta at disse henger sammen. Det er først 2 TLSA, deretter HINFO. Hvis vi dekoder data fra første DNS spørring med `base58` i får vi ut `Crypto.Cipher.MODE_CFB` - en mode brukt i AES. For å bruke MODE_CFB trenger vi iv/seed. Vi ser at neste DNS spørring er 16 byte, og oppgaven heter `Rijndael 128` hvor 128-bit AES bruker 16byte IV. Tredje melding kan da være dataene. Vi kan da forsøke AES dekryptering med:
- første dns spørring som mode (MODE_CFB) (se [cyberchef](https://gchq.github.io/CyberChef/#recipe=From_Base58('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',true)&input=S0prNFBFNVRjUEYydzlZOVdqQ3BxVGZKRXBvdTdQ))
- andre dns spørring som IV (se [cyberchef](https://gchq.github.io/CyberChef/#recipe=From_Base58('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',true)To_Hex('Space',0)&input=OGtFb3RxSm9vd3BnVHBnZEFNZ00xcw))
- tredje dns spørring som data
- som nøkkel bruker vi `SuperVinter2020!` fra powershell kommandoen (som også er 16 byte)

Vi ser at data-delen i tredje melding har flere dots (.) med DNS. Dette er fordi det er en begrensning på gyldig DNS med 63 byte per octete. Ved å fjerne dot (.) i data delen kan vi dekode meldingen:

``` python
from Crypto.Cipher import AES
key = "SuperVinter2020!"
mode = AES.MODE_CFB
iv = bytes.fromhex("3eb9766834271c260e15841ad5c54c7a")
data = bytes.fromhex("53432fc930163d8c11fbe01aa1a73408e0ccd3a84085305ae0eb1bb1ea0da4bfffb4cd7c058a66ba108ae63b3bf80fb15ba3351fc54e09ab925fb3ef23f191cb")
cipher = AES.new(key, mode, iv)
print(cipher.decrypt(data).decode())
```

Vi får ut `system_uuid: df039c9e-2648-444d-b266-00711956084e` som kanskje er IDen som sier hvilken datamaskin som er kompromittert. HINFO kan være angripers måte å si "host info". Etter pakke 38 dukker CNAME opp istede for HINFO, men samme mønster med session-id og TLSA.

Vi bygger ut skriptet og organiserer dataene etter session-id og har da AES mode, IV og data. Det brukes også ECB som ikke bruker IV/seed/nonce. Vi får da ut en del tekst, også begynner det mer enkodet data. Kjører [aes_decode1.py](aes_decode1.py):
```
system_uuid: df039c9e-2648-444d-b266-00711956084e                                                                      
ip-address: 10.101.53.246                                                                                              
hostname: WIN-KYLLING110.CTF                                                                                           
username: kylling17                                                                                                    
uptime: 58507                                                                                                          
mem: 12355191 bytes                                                                                                    
space: 174530145 bytes                                                                                                 
free: 124535278 bytes                                                                                                  
os: windows 7                                                                                                          
drive: c: d: e:                                                                                                        
payload details:                                                                                                       
hash.type: md5                                                                                                         
guess.charset: a-z0-9                                                                                                  
guess.len: up to 8 bytes                                                                                               
aGFzaGVkX3BheWxvYWRfY2h1bms6NzM1NGJjODE4ODdmZDAwOWEwNmYwOTQxN2Q0YTBkNTUgbmV4dDouMEt6Mm5IV3QuY3Rm        
aGFzaGVkX3BheWxvYWRfY2h1bms6MGU0NmVhNjBiYWM0MGVlZTVhYjhlMDUxNDYwMWI3ZTkgbmV4dDouSHhMNWVxQS5jdGY=
aGFzaGVkX3BheWxvYWRfY2h1bms6NWE3MDliYmRiODU4NzEzZWY2MjA2OTg2N2ZjYmE1N2EgbmV4dDouOExCMXl2cy5jdGY=
aGFzaGVkX3BheWxvYWRfY2h1bms6OWZlMTMxNTQyMTkxYjg2MjljNGY2YzA2ZTQxMzQ4MTYgbmV4dDouTEw0WUZPLmN0Zg==
aGFzaGVkX3BheWxvYWRfY2h1bms6OWZlMTMxNTQyMTkxYjg2MjljNGY2YzA2ZTQxMzQ4MTYgbmV4dDouU2JjUDRNMjBYRjBWRC5jdGY=
aGFzaGVkX3BheWxvYWRfY2h1bms6ZDliYTE0YjM5N2ZjYzZiMDcxYTNkNzAwZjE0OWU5NmEgbmV4dDouR2ZERG94aWViWUJtMS5jdGY=
aGFzaGVkX3BheWxvYWRfY2h1bms6OWZlMTMxNTQyMTkxYjg2MjljNGY2YzA2ZTQxMzQ4MTYgbmV4dDouNjZFeFVPLmN0Zg==
aGFzaGVkX3BheWxvYWRfY2h1bms6Y2U5NzUyNDA1Mzc0Yzg0OWM3NzgwZDUyZmE1MTdmYTAgbmV4dDouV0N4RXZ3clpTTi5jdGY=
aGFzaGVkX3BheWxvYWRfY2h1bms6NTA1NTgwYWYwZGUzMWJlNGE2Y2IwNDY4MmQzODYyNzYgbmV4dDouTXRwZm5ESGt2VEVELmN0Zg==
aGFzaGVkX3BheWxvYWRfY2h1bms6MjZjMjE0OWVlMDE0MWI5M2Q5M2I3NjQ2ZGI2N2EyODQgbmV4dDouS0szcG0wWWxmMmtlQjZvLmN0Zg==
aGFzaGVkX3BheWxvYWRfY2h1bms6ZjJkZDA3Njg4ZDA2OGQ4ZjQ4MDU0MmU3Njk1NTlkYjEgbmV4dDoucVhLaXFDWC5jdGY=
aGFzaGVkX3BheWxvYWRfY2h1bms6NWRhYjA2OTUxZTM0YTAwYjJlZDkzNWQ5N2QzNTgxNzIgbmV4dDouZ2pQYVp0bi5jdGY=
aGFzaGVkX3BheWxvYWRfY2h1bms6OWZlMTMxNTQyMTkxYjg2MjljNGY2YzA2ZTQxMzQ4MTYgbmV4dDoudEJjQU9Vb1dvZ3VtU2NhLmN0Zg==

... osv
```

Første del av dataene er informasjon om hosten (HINFO) som hostnavn, brukernavn, uptime, operativsystem o.l. Deretter kommer det `payload details` som beskriver den videre dataen. Vi ser at det brukes ord som `hash.type`, `guess.charset` og `guess.len`. Dette peker kraftig mot knekking av hash. Vi ser også at payloaden har to identiske partier `aGFzaGVkX3BheWxvYWRfY2h1bms6` og `gbmV4dDou`. Dette ser ut som base64, og vi kan dekode payloadene med et modifisert script [aes_decode2.py](aes_decode2.py):
```
b'hashed_payload_chunk:7354bc81887fd009a06f09417d4a0d55 next:.0Kz2nHWt.ctf'      
b'hashed_payload_chunk:0e46ea60bac40eee5ab8e0514601b7e9 next:.HxL5eqA.ctf'        
b'hashed_payload_chunk:5a709bbdb858713ef62069867fcba57a next:.8LB1yvs.ctf'        
b'hashed_payload_chunk:9fe131542191b8629c4f6c06e4134816 next:.LL4YFO.ctf'       
b'hashed_payload_chunk:9fe131542191b8629c4f6c06e4134816 next:.SbcP4M20XF0VD.ctf'  
b'hashed_payload_chunk:d9ba14b397fcc6b071a3d700f149e96a next:.GfDDoxiebYBm1.ctf'  
b'hashed_payload_chunk:9fe131542191b8629c4f6c06e4134816 next:.66ExUO.ctf'    
b'hashed_payload_chunk:ce9752405374c849c7780d52fa517fa0 next:.WCxEvwrZSN.ctf'    
b'hashed_payload_chunk:505580af0de31be4a6cb04682d386276 next:.MtpfnDHkvTED.ctf'   
b'hashed_payload_chunk:26c2149ee0141b93d93b7646db67a284 next:.KK3pm0Ylf2keB6o.ctf'
...
b'hashed_payload_chunk:b6d767d2f8ed5d21a44b0e5886680cb9 next:stop'

... osv
```

Vi ser at det er en lang liste med `hashed_payload_chunk` og `next`. Dette er en lenket liste, hvor hashed_payload_chunk er payload-dataen hashet med `md5`, og next viser til hvilken payload er neste ut fra domenet. For ett av domenene får vi ut `next:stop`, dette betyr kanskje at det er slutten på den lenka lista.

Det ser ut til at vi må knekke hashene for å komme videre, men vi har fått god hjelp i HINFO meldingene rundt payload details. Modifiserer scriptet så det kun hiver ut md5sums, lagrer disse i en fil (md5hash.txt) og fyrer opp `hashcat` med en inkrementell knekking opp til `guess.len` (8) hvor vi bruker `guess.charset` som er HEX, altså `?h` maske og MD5. Hashcat spiser md5summene på en hutgammel i7-2600K i løpet av knappe 45 sekunder:
```
$ hashcat -m 0 -a 3 -i md5hash.txt '?h?h?h?h?h?h?h?h'
...

$ hashcat -m 0 -a 3 -i md5hash.txt '?h?h?h?h?h?h?h?h' --show
264e9ad73f34250168b4cc0392486af5:222b2232
c3c84966c4e8f403e961d3efdd05d6e3:3238222b
a71e07b17ed60b04411bf557ed07a9a2:2b223338
7354bc81887fd009a06f09417d4a0d55:22363322
5dab06951e34a00b2ed935d97d358172:39222b22
e6752f243cb17c185ec934ce30bac6e8:2b223239
f39ea795c523e618027fec6d85d4d45d:22333222
183686e7a32f111d834bdf094fd54e6c:3330222b
bdeed3637c49741f1faa6b7d8c1729b7:37222b22
d0fc11dcd42a2121c619e4481a1558c8:2b223332
b6d767d2f8ed5d21a44b0e5886680cb9:22
0e56397eea84d125c39d2aaff1f3c6a3:2b223337
3960d618e6ea702f15ab2d8c352aec52:3262222b
24dda8aa99d9752ff2d4129ef2343946:3732222b
f4c851731fdf802feab9182c70dfe956:222b2237
24cfc9b651992f32cf7803d82e09ccf7:22333922
eeb3ab43e0bd7d340ca5eac39f2a1114:2b223633
7df92aceed1a0b35942b5fbac9e57954:3331222b
aaf6727a74c653376d760a1c7116f99f:2b223262
5a709bbdb858713ef62069867fcba57a:2b223331
9e180eaaf9454644b35cadc410a3754a:3338222b
3b74f08219f4cc473b987e0b23d98a6e:22333722
ce9752405374c849c7780d52fa517fa0:32222b22
8e033ccb1c936b4d58dbdb0c78392863:22373222
e48a3b5a3e1a3e576ed62917d9053e46:22326222
747b1b2227699b57147a798258033608:2b223238
b9b14067b2adfa5ec42e1c1c1383c27b:222b2236
9fe131542191b8629c4f6c06e4134816:222b2233
ca794ca93d9ce264e47c97712ee5e38a:22333522
9e82895631b6b2699a08a03b47cfc938:35222b22
9a06b7788dc2b97124f0782d710be3ab:36222b22
25fe270d0dcd04734fe60d04f5900bac:2b223335
5e1a0226f44d668abaa374eb1ad22098:22333622
f2dd07688d068d8f480542e769559db1:3638222b
26c2149ee0141b93d93b7646db67a284:22333122
605c5fe517ff1d973bb0503f834d00f0:3337222b
f0274d52f431dd9a71ba49d1c92a9b61:2b223638
c04a0e6f69f89da3379770be9c13c21c:22363822
57289f9352a045ae079d5dc3d393a989:33222b22
7ed45530a9bd04b03820ec463c743809:30222b22
aedfd44bf7702eb0e9ec033b852d1c65:2b223334
d9ba14b397fcc6b071a3d700f149e96a:22323822
b9ec3686ada73ab967d866872bb031c3:62222b22
d8399c4ee3e25abe253730e4723e903b:3334222b
db2261f439ebacc4eecc5b5f899f2149:2b223339
c01fce73ef3985c63cbc389eefd9c1a7:22323922
c8e1e90296d8ebca19ee8e10ab27893d:2b223330
c8b32dce891854ceefca7c7d056e172f:3335222b
9b4b93675af5d3d44a46b6acec242258:22333022
fccd276319fdc9d5f1ab369419e1da8c:31222b22
91c7fc5a1e49c7dc2b4646074c4a6a0a:2b223732
505580af0de31be4a6cb04682d386276:3239222b
0e46ea60bac40eee5ab8e0514601b7e9:38222b22
4a76531f66c746f34a88dc66920f7252:3633222b
373b83118b8351f74af5de7ac270b2eb:3332222b
```

Vi lagrer md5sums i scriptet, og oversetter payload til det som kom ut. Vi lagrer alle payloads og next domain fra nåværende domain. Da kan vi følge den lenka lista og bygge riktig payload. Vi finner den lengste kjeden og bygger en kontinuerlig payload ved å følge de knekte hashene. Dette gjøres i nok en versjon av skriptet [aes_decode3.py](aes_decode3.py). Payloaden er:
```
223633222b223638222b223732222b223238222b223336222b223339222b223239222b223262222b223633222b223638222b223732222b223238222b223337222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223337222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223332222b223333222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223338222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223333222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223334222b223338222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223334222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223338222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223334222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223337222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223338222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223330222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223339222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223330222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223337222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223336222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223330222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223337222b223239222b223262222b223633222b223638222b223732222b223238222b223334222b223339222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223330222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223337222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223332222b223335222b22323922
```

Vi ser et mønster hvor veldig mye gjentas. Vi kunne forsøkt å dekode dette ved å ta 2 tegn som decimal ascii, men havner fort i ikke-printbar scope av ASCII. Det er også en `b` der som peker mot HEX. Fra starten har vi `22363322` som ville blitt `"63"`. Vi forsøker å gjøre om fra HEX til bytes:

``` python
payload = "223633222b223638222b223732222b223238222b223336222b223339222b223239222b223262222b223633222b223638222b223732222b223238222b223337222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223337222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223332222b223333222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223338222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223333222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223334222b223338222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223334222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223338222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223334222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223337222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223331222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223338222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223330222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223339222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223330222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223337222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223336222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223330222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223337222b223239222b223262222b223633222b223638222b223732222b223238222b223334222b223339222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223335222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223330222b223332222b223239222b223262222b223633222b223638222b223732222b223238222b223335222b223330222b223239222b223262222b223633222b223638222b223732222b223238222b223339222b223337222b223239222b223262222b223633222b223638222b223732222b223238222b223331222b223332222b223335222b22323922"

bytes.fromhex(payload).decode()

'"63"+"68"+"72"+"28"+"36"+"39"+"29"+"2b"+"63"+"68"+"72"+"28"+"37"+"31"+"29"+"2b"+"63"+"68"+"72"+"28"+"37"+"31"+"29"+"2b"+"63"+"68"+"72"+"28"+"31"+"32"+"33"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"31"+"29"+"2b"+"63"+"68"+"72"+"28"+"39"+"38"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"33"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"31"+"29"+"2b"+"63"+"68"+"72"+"28"+"34"+"38"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"35"+"29"+"2b"+"63"+"68"+"72"+"28"+"31"+"30"+"32"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"34"+"29"+"2b"+"63"+"68"+"72"+"28"+"39"+"38"+"29"+"2b"+"63"+"68"+"72"+"28"+"31"+"30"+"32"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"35"+"29"+"2b"+"63"+"68"+"72"+"28"+"31"+"30"+"31"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"34"+"29"+"2b"+"63"+"68"+"72"+"28"+"39"+"37"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"31"+"29"+"2b"+"63"+"68"+"72"+"28"+"39"+"38"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"30"+"29"+"2b"+"63"+"68"+"72"+"28"+"39"+"39"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"30"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"32"+"29"+"2b"+"63"+"68"+"72"+"28"+"31"+"30"+"32"+"29"+"2b"+"63"+"68"+"72"+"28"+"39"+"37"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"36"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"35"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"30"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"35"+"29"+"2b"+"63"+"68"+"72"+"28"+"39"+"37"+"29"+"2b"+"63"+"68"+"72"+"28"+"34"+"39"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"35"+"29"+"2b"+"63"+"68"+"72"+"28"+"31"+"30"+"32"+"29"+"2b"+"63"+"68"+"72"+"28"+"35"+"30"+"29"+"2b"+"63"+"68"+"72"+"28"+"39"+"37"+"29"+"2b"+"63"+"68"+"72"+"28"+"31"+"32"+"35"+"29"' 
```

Dette er en faktisk python kode som en streng som setter sammen data som strenger. Vi kan få dette gjort om til en streng ved å bruke [eval](https://docs.python.org/3/library/functions.html#eval):
``` python
eval(bytes.fromhex(payload))

'636872283639292b636872283731292b636872283731292b63687228313233292b636872283531292b636872283938292b636872283533292b636872283531292b636872283438292b636872283535292b63687228313032292b636872283534292b636872283938292b63687228313032292b636872283535292b63687228313031292b636872283534292b636872283937292b636872283531292b636872283938292b636872283530292b636872283939292b636872283530292b636872283532292b63687228313032292b636872283937292b636872283536292b636872283535292b636872283530292b636872283535292b636872283937292b636872283439292b636872283535292b63687228313032292b636872283530292b636872283937292b6368722831323529'
```

Vi er litt tilbake til den forrige, hvor vi igjen ser `b` og gjør om fra HEX til bytes:
```
bytes.fromhex(eval(bytes.fromhex(payload)))

b'chr(69)+chr(71)+chr(71)+chr(123)+chr(51)+chr(98)+chr(53)+chr(51)+chr(48)+chr(55)+chr(102)+chr(54)+chr(98)+chr(102)+chr(55)+chr(101)+chr(54)+chr(97)+chr(51)+chr(98)+chr(50)+chr(99)+chr(50)+chr(52)+chr(102)+chr(97)+chr(56)+chr(55)+chr(50)+chr(55)+chr(97)+chr(49)+chr(55)+chr(102)+chr(50)+chr(97)+chr(125)'
```

På nytt har vi en python kode, og vi kjører den med eval:
```
eval(bytes.fromhex(eval(bytes.fromhex(payload))))

'EGG{3b5307f6bf7e6a3b2c24fa8727a17f2a}'
```

Og vi har kommet til veis ende i et ganske dypt kaninhull :-)

Flagg: `EGG{3b5307f6bf7e6a3b2c24fa8727a17f2a}`