En årvåken påskekylling har oppdaget en stor økning i DNS trafikk ut fra et klient-segment. På en av datamaskinene fant han en kommando i sysmon-loggen:

`cmd /c powershell.exe -w hidden -nop -enc cwBsAGUAZQBwACAAMwAwADAACgAkAGEAPQAiAHIAaQBqACIACgAkAHAAdABoAD0AJABMAG8AYwBhAGwAOgBIAE8ATQBFACsAIgBcAEEAcABwAEQAYQB0AGEAXABMAG8AYwBhAGwAXAAiAAoAZABpAHIAIAAkAHAAdABoAAoAJABlAD0AIgBuAGQAYQAiAAoAJABrAGUAeQA9AFsAUwB5AHMAdABlAG0ALgBUAGUAeAB0AC4ARQBuAGMAbwBkAGkAbgBnAF0AOgA6AFUAbgBpAGMAbwBkAGUALgBHAGUAdABTAHQAcgBpAG4AZwAoAFsAUwB5AHMAdABlAG0ALgBDAG8AbgB2AGUAcgB0AF0AOgA6AEYAcgBvAG0AQgBhAHMAZQA2ADQAUwB0AHIAaQBuAGcAKAAnAFUAMwBWAHcAWgBYAEoAVwBhAFcANQAwAFoAWABJAHkATQBEAEkAdwBJAFEAbwA9ACcAKQApAAoAJABzAD0AIgBlAGwAIgAKAFMAdABhAHIAdAAtAFAAcgBvAGMAZQBzAHMAIABlAHYAaQBsAF8AcwB2AGMAaABvAHMAdAAuAGUAeABlACAALQBlAG4AYwBvAGQAaQBuAGcAIAA1ADgAIAAtAGsAZQB5ACAAJABrAGUAeQAgAC0AYwByAHkAcAB0AG8AIAAkAGEAJABlACQAcwA=`

Etter en kort analyse fikk påskekyllingen startet på en rapport. 

– Her har de jo brukt den sterke krypteringen fra 2002, mumler han til seg selv mens han også skriver ned start verdiene med tilhørende driftsmodus. Han tar også med passordet og enkodingen han fant i powershell-kommandoen. Med disse greier han å dekryptere meldingene. 

Det siste han rekker å notere inn i rapporten før han må løpe i et viktig møte er at det virker som angriper har fragmentert meldingen sin på en noe uvanlig måte.

Kan du fullføre analysen og finne ut om angriper greide å eksfiltrere ut et EGG?

Lever `EGG{32-byte hex}`

Oppdatering: Påskeharen oppfordrer alle påskekyllinger til å bruke python for å dekode AES med MODE_CFB siden segment_size = 8 er satt som default i Crypto.Cipher.AES, mens i andre implementasjoner av AES-128 med MODE_CFB så vil segment size være satt til 128 (som CyberChef).

Fil: [dns.pcap.7z](dns.pcap.7z)

[HINT](HINT.md)