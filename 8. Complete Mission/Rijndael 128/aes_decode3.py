from scapy.all import *
from Crypto.Cipher import AES
import base58
import base64

key = "SuperVinter2020!"

modes = {
    "Crypto.Cipher.MODE_ECB": 1,
    "Crypto.Cipher.MODE_CBC": 2,
    "Crypto.Cipher.MODE_CFB": 3,
}

sessions = {}
packets = rdpcap('dns.pcap')
for i in range(len(packets)):
    if packets[i].haslayer(DNS):
        dns = packets[i].getlayer(DNS)
        r = dns.qd.qname.decode("utf-8")
        data = "".join(r.split(".")[0:-4])
        idd = r.split(".")[-4]

        if idd not in sessions:
            sessions[idd] = {}

        domain = "." + ".".join(r.split(".")[-3:])[:-1]
        sessions[idd]["domain"] = domain

        if dns.qd.qtype == 52:  # TLSA
            tlsa_data = base58.b58decode(data)
            if len(tlsa_data) == 16:  # IV
                sessions[idd]["iv"] = tlsa_data
            else:
                mode = modes[tlsa_data.decode("utf-8")]
                sessions[idd]["mode"] = mode
        else:
            if "data" not in sessions[idd]:
                sessions[idd]["data"] = []
            sessions[idd]["data"].append(data)


hhes = """
264e9ad73f34250168b4cc0392486af5:222b2232
c3c84966c4e8f403e961d3efdd05d6e3:3238222b
a71e07b17ed60b04411bf557ed07a9a2:2b223338
7354bc81887fd009a06f09417d4a0d55:22363322
5dab06951e34a00b2ed935d97d358172:39222b22
e6752f243cb17c185ec934ce30bac6e8:2b223239
f39ea795c523e618027fec6d85d4d45d:22333222
183686e7a32f111d834bdf094fd54e6c:3330222b
bdeed3637c49741f1faa6b7d8c1729b7:37222b22
d0fc11dcd42a2121c619e4481a1558c8:2b223332
b6d767d2f8ed5d21a44b0e5886680cb9:22
0e56397eea84d125c39d2aaff1f3c6a3:2b223337
3960d618e6ea702f15ab2d8c352aec52:3262222b
24dda8aa99d9752ff2d4129ef2343946:3732222b
f4c851731fdf802feab9182c70dfe956:222b2237
24cfc9b651992f32cf7803d82e09ccf7:22333922
eeb3ab43e0bd7d340ca5eac39f2a1114:2b223633
7df92aceed1a0b35942b5fbac9e57954:3331222b
aaf6727a74c653376d760a1c7116f99f:2b223262
5a709bbdb858713ef62069867fcba57a:2b223331
9e180eaaf9454644b35cadc410a3754a:3338222b
3b74f08219f4cc473b987e0b23d98a6e:22333722
ce9752405374c849c7780d52fa517fa0:32222b22
8e033ccb1c936b4d58dbdb0c78392863:22373222
e48a3b5a3e1a3e576ed62917d9053e46:22326222
747b1b2227699b57147a798258033608:2b223238
b9b14067b2adfa5ec42e1c1c1383c27b:222b2236
9fe131542191b8629c4f6c06e4134816:222b2233
ca794ca93d9ce264e47c97712ee5e38a:22333522
9e82895631b6b2699a08a03b47cfc938:35222b22
9a06b7788dc2b97124f0782d710be3ab:36222b22
25fe270d0dcd04734fe60d04f5900bac:2b223335
5e1a0226f44d668abaa374eb1ad22098:22333622
f2dd07688d068d8f480542e769559db1:3638222b
26c2149ee0141b93d93b7646db67a284:22333122
605c5fe517ff1d973bb0503f834d00f0:3337222b
f0274d52f431dd9a71ba49d1c92a9b61:2b223638
c04a0e6f69f89da3379770be9c13c21c:22363822
57289f9352a045ae079d5dc3d393a989:33222b22
7ed45530a9bd04b03820ec463c743809:30222b22
aedfd44bf7702eb0e9ec033b852d1c65:2b223334
d9ba14b397fcc6b071a3d700f149e96a:22323822
b9ec3686ada73ab967d866872bb031c3:62222b22
d8399c4ee3e25abe253730e4723e903b:3334222b
db2261f439ebacc4eecc5b5f899f2149:2b223339
c01fce73ef3985c63cbc389eefd9c1a7:22323922
c8e1e90296d8ebca19ee8e10ab27893d:2b223330
c8b32dce891854ceefca7c7d056e172f:3335222b
9b4b93675af5d3d44a46b6acec242258:22333022
fccd276319fdc9d5f1ab369419e1da8c:31222b22
91c7fc5a1e49c7dc2b4646074c4a6a0a:2b223732
505580af0de31be4a6cb04682d386276:3239222b
0e46ea60bac40eee5ab8e0514601b7e9:38222b22
4a76531f66c746f34a88dc66920f7252:3633222b
373b83118b8351f74af5de7ac270b2eb:3332222b
"""
hashes = {}

payloads = {}
domains = {}
for h in hhes.split("\n"):
    if len(h) < 1:
        continue
    h = h.strip()
    z = h.split(":")
    hashes[z[0]] = z[1]

for idd in sessions:
    session = sessions[idd]
    iv = 0
    if "iv" in session:
        iv = session["iv"]
    mode = session["mode"]
    domain = session["domain"]

    for data in session["data"]:
        if "iv" in session:
            cipher = AES.new(key, mode, iv)
        else:
            cipher = AES.new(key, mode)
        dekodet = cipher.decrypt(base58.b58decode(data))
        dekodet = dekodet[:-dekodet[-1]]  # strip PKCS#7 padding
        if b":" not in dekodet:  # base64 enkodet
            p = base64.b64decode(dekodet).decode("utf-8")
            m5 = p.split(" ")[0].split(":")[1]
            _next = p.split(" ")[1].split(":")[1]
            payloads[domain] = hashes[m5]
            domains[domain] = _next
#        else:
#            print(dekodet.decode())


def follow(d):
    dd = []
    while True:
        if d == "stop":
            break
        dd.append(d)
        d = domains[d]
    return dd


longest = 0
l = 0
for d in domains:
    chain = follow(d)
    if len(chain) > l:
        longest = d
        l = len(chain)

d = longest
payload = ""
while True:
    if d == "stop":
        break
    payload += payloads[d]
    d = domains[d]

print(payload)
