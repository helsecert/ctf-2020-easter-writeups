from scapy.all import *
from Crypto.Cipher import AES
import base58

key = "SuperVinter2020!"

modes = {
    "Crypto.Cipher.MODE_ECB": 1,
    "Crypto.Cipher.MODE_CBC": 2,
    "Crypto.Cipher.MODE_CFB": 3,
}

sessions = {}
packets = rdpcap('dns.pcap')
for i in range(len(packets)):
    if packets[i].haslayer(DNS):
        dns = packets[i].getlayer(DNS)
        r = dns.qd.qname.decode("utf-8")
        data = "".join(r.split(".")[0:-4])
        idd = r.split(".")[-4]

        if idd not in sessions:
            sessions[idd] = {}

        domain = "." + ".".join(r.split(".")[-3:])[:-1]
        sessions[idd]["domain"] = domain

        if dns.qd.qtype == 52:  # TLSA
            tlsa_data = base58.b58decode(data)
            if len(tlsa_data) == 16:  # IV
                sessions[idd]["iv"] = tlsa_data
            else:
                mode = modes[tlsa_data.decode("utf-8")]
                sessions[idd]["mode"] = mode
        else:
            if "data" not in sessions[idd]:
                sessions[idd]["data"] = []
            sessions[idd]["data"].append(data)

for idd in sessions:
    session = sessions[idd]
    iv = 0
    if "iv" in session:
        iv = session["iv"]
    mode = session["mode"]
    domain = session["domain"]

    for data in session["data"]:
        if "iv" in session:
            cipher = AES.new(key, mode, iv)
        else:
            cipher = AES.new(key, mode)
        dekodet = cipher.decrypt(base58.b58decode(data))
        dekodet = dekodet[:-dekodet[-1]]  # strip PKCS#7 padding
        print(dekodet.decode())
