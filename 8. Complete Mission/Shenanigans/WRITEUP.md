WRITEUP
=======

Oppgave
-------
En flyvende påskekylling har klart å hente ut en ZIP-fil som ble eksfiltrert fra nettverket. Logg fra maskinen ZIP-filen ble sendt fra indikerer at det skal ligge en fil, `flagg.docx`, i arkivet, men kyullingen trenger åpenbart mer kaffe for han ser ikke noe flagg. I minnedumpen fra maskinen finner han strengene `test123qwe` og `ARCFOUR`. 

Han mistenker at aktøren, som sikkert er et vanedyr, har presset seg gjennom en solnedgang. Kan du hjelpe?

Lever EGG{32-byte hex}

Løsning
-------

Hintet om å ha "presset seg gjennom en solnedgang" er en referanse til oppgaven "Påskeegg i solnedgang" som bruker `zlib` for komprimering.

Zip-filen som utleveres har fire filer i seg:

```
Size     Compressed   Name
--------------------------
 16133        15361   nettverksdiagram.png
345606       328983   New Cyber Threat Intel Strategy.pptx
  4271         3589   Questionnaire - Easter Bunny Workplace Survey.docx
  5051         4372   Ressursoversikt.xlsx
--------------------------
```

Hvis vi pakker ut disse filene og ser på dem sier alle, på en eller annen måte, at flagget ikke ligger der. Vi kan undersøke filene nærmere eller vi kan se litt nærmere på Zip-filen vi fikk utlevert. La oss starte med det siste.

Output fra f.eks. 7zip forteller oss at de komprimerte filene tar opp totalt `352305` bytes, mens filen er på `356938` bytes. Det gir oss et overhead i Zip-formatet på `4633` bytes - over 4kB overhead for fire filer. Dersom vi pakker ut filene i arkivet og pakker dem inn i en ny Zip-fil ser vi et overhead på under 1kB, så her foregår det noe muffens.

La oss ta en Kjapp(TM) titt på Zip-formatet: https://en.wikipedia.org/wiki/Zip_(file_format)#Structure.

Vi ser at Zip består av fire forskjellige datastrukturer: `Local file header`, `Data descriptor`, `Central directory file header` og `End of central directory`.

Tre av disse (`local file header`, `central directory file header`, `end of central directory`) kan holde ekstra data, i feltene `extra field` eller i `file comment`. For å se om det ligger gjemt data i disse feltene må vi parse Zip-formatet. Heldigvis gjør Python dette lett for oss:

`parse.py:`
```python
#!/usr/bin/env python3

from zipfile import ZipFile
import sys

def parse(path):
    zip = ZipFile(path)
    for z in zip.infolist():
        print("filename: {}".format(z.filename))
        print(" comment: {}".format(z.comment))
        print("   extra: {}".format(z.extra))
        print()

def die(s):
    print(s)
    sys.exit(-1)

if __name__=="__main__":
    if len(sys.argv) != 2:
        die("usage: {} <file to parse>".format(sys.argv[0]))
    parse(sys.argv[1])
```
```bash
$ python3 parse.py ~/repo/ctf-2020/oppgaver/8_COMPLETE_MISSION/zip_shenanigans/task.zip 
filename: nettverksdiagram.png
 comment: b''
   extra: b'UT\x05\x00\x03/\xcbd^ux\x0b\x00\x01\x04\xe8\x03\x00\x00\x04\xe8\x03\x00\x00'

filename: New Cyber Threat Intel Strategy.pptx
 comment: b''
   extra: b'UT\x05\x00\x03e\xda\x81^ux\x0b\x00\x01\x04\xe8\x03\x00\x00\x04\xe8\x03\x00\x00'

filename: Questionnaire - Easter Bunny Workplace Survey.docx
 comment: b''
   extra: b'UT\x05\x00\x03e\xda\x81^ux\x0b\x00\x01\x04\xe8\x03\x00\x00\x04\xe8\x03\x00\x00'

filename: Ressursoversikt.xlsx
 comment: b''
   extra: b'UT\x05\x00\x03e\xda\x81^ux\x0b\x00\x01\x04\xe8\x03\x00\x00\x04\xe8\x03\x00\x00'
```

Kommentarfeltene er tomme og extra-feltet inneholder veldig lite data. Litt googling forteller oss at disse sannsynligvis er knyttet til timestamps, og det er uansett ikke nok data til å veie opp for det vi har sett.

Det er på tide å se enda nærmere på datastrukturene i Zip-filen. `Zipfile`-modulen i Python lar oss ikke parse og håndtere dataene så detaljert som vi vil, så vi må inn med et annet verktøy. Her kan vi kode opp noe eget for å parse filen eller vi kan bruke et rammeverk som [Kaitai Struct](https://kaitai.io/). Dersom vi forsøker å parse den utleverte Zip-filen med [Zip-eksempelet til Kaitai](https://formats.kaitai.io/zip/index.html) (i Python) får vi en parsefeil: 

```python
Exception: unexpected fixed contents: got b'k-', was waiting for b'PK'
```

Vi får denne feilen fordi Kaitai forventer at alle datastrukturene i Zip-filen ligger sekvensielt, selv om det ikke er nødvendig i filformatet. Der Kaitai forventer å finne en header for en datastruktur (som alle begynner med bytene `PK`) finner den `k-` i stedet.

Vi kan redigere eksempelet fra Kaitai til å printe offset i filen før den krasjer, ved å legge inn 
```python
            print("offset: {}".format(self._io.pos()))
```
rett før linje 279.

Kjører vi koden (`kaitai_parse.py`) ser vi at offset `352663` er hvor `Kaitai` ikke klarer å parse. Vi kan teste å dekryptere (og dekomprimere) her:

`decrypt_offset.py:`
```python
#!/usr/bin/env python3

import sys
import zlib
from Crypto.Cipher import ARC4

OFFSET = 352663
KEY = "test123qwe"

def work(path):
    with open(path, "rb") as f:
        buf = f.read()
    buf = buf[OFFSET:]

    c = ARC4.new(KEY)
    dec = c.decrypt(buf)
    decomp = zlib.decompress(dec)

    newpath = "{}.decrypted".format(path)

    with open(newpath, "wb") as f:
        f.write(decomp)

    print("[+] file stored at: {}".format(newpath))

if __name__=="__main__":
    if len(sys.argv) != 2:
        die("usage: {} <file to parse>".format(sys.argv[0]))
    work(sys.argv[1])
```

Resultatet er en Docx-fil som inneholder flagget :)

Dersom vi mistenkte tidligere at det lå data rett i Zip-filen kunne vi forsøkt en slags bruteforcing med RC4-nøkkelen som ble oppgitt - prøve å dekryptere fra alle offsets til vi fant noe interessant. Her måtte vi sett etter en `zlib` header, noe `libmagic` kan hjelpe oss med.

En mer tungvindt løsning er å skrive kode som parser hele Zip-filen og redegjør for alle bytes i strukturene i formatet. Det vil raskt vise om det ligger ekstra data lagret i Zip-filen.

I en Zip-fil kan vi lagre data flere steder uten at standard-verktøy reagerer på det - mellom `local file header`-strukturer, mellom siste `local file header` og første `central directory file header`-struktur og før første `local file header`. Dersom vi forsøker å lagre ekstra data etter `end of central directory` vil flere verktøy reagere på at det ligger data etter Zip-arkivet, og dersom vi plasserer data mellom siste `central directory file header` og `end of central directory` treffer vi også på parsing errors i forskjellige verktøy som forventer at disse ligger rett etter hverandre.

Å plassere data før første `local file header` har en ekstra fin bonus: de første bytene på filen viser ikke at det er en Zip-fil. Siden Zip parses fra slutten (for å finne `end of central directory`) spiller det ingen rolle hvordan filen starter, noe vi ser med jevne mellomrom at trusselaktører utnytter fordi automatiske analysesystemer (sandkasser mm.) gjerne analyserer en fil basert på filsignaturen i starten på filen.

Uansett hvor vi setter inn data må offset i de andre strukturene oppdateres tilsvarende.

I denne oppgaven var det plassert ekstra data mellom siste `local file header` og første `central directory file header`.

Oppgaven er en litt mer raffinert versjon av en metode som ble brukt for å skjule kryptert payload under installasjon av en RAT.