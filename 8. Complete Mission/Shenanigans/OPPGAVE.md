Oppgave
=======
En flyvende påskekylling har klart å hente ut en ZIP-fil som ble eksfiltrert fra nettverket. Logg fra maskinen ZIP-filen ble sendt fra indikerer at det skal ligge en fil, `flagg.docx`, i arkivet, men kyullingen trenger åpenbart mer kaffe for han ser ikke noe flagg. I minnedumpen fra maskinen finner han strengene `test123qwe` og `ARCFOUR`. 

Han mistenker at aktøren, som sikkert er et vanedyr, har presset seg gjennom en solnedgang. Kan du hjelpe?

Lever `EGG{32-byte hex}`

Fil: [task.zip](task.zip)

[HINT](HINT.md)