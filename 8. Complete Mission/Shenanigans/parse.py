#!/usr/bin/env python3

from zipfile import ZipFile
from Zip import Zip
import sys

def parse(path):
    zip = Zip.from_file(path)
    """zip = ZipFile(path)
    for z in zip.infolist():
        print("filename: {}".format(z.filename))
        print(" comment: {}".format(z.comment))
        print("   extra: {}".format(z.extra))
        print()"""

def die(s):
    print(s)
    sys.exit(-1)

if __name__=="__main__":
    if len(sys.argv) != 2:
        die("usage: {} <file to parse>".format(sys.argv[0]))
    parse(sys.argv[1])