#!/usr/bin/env python3

import sys
import zlib
from Crypto.Cipher import ARC4

OFFSET = 352663
KEY = "test123qwe"

def work(path):
    with open(path, "rb") as f:
        buf = f.read()
    buf = buf[OFFSET:]

    c = ARC4.new(KEY)
    dec = c.decrypt(buf)
    decomp = zlib.decompress(dec)

    newpath = "{}.decrypted".format(path)

    with open(newpath, "wb") as f:
        f.write(decomp)

    print("[+] file stored at: {}".format(newpath))

if __name__=="__main__":
    if len(sys.argv) != 2:
        die("usage: {} <file to parse>".format(sys.argv[0]))
    work(sys.argv[1])