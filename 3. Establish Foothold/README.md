Oppgavene for `Establish Foothold` har følgende tiltenkte rekkefølge:

- [Eksklusiv krypto](Eksklusiv\ krypto/OPPGAVE.md)
- [Eksklusiv, differensiert krypto](Eksklusiv,\ differensiert\ krypto/OPPGAVE.md)
- [Good Crypto Dog](Good\ Crypto\ Dog/OPPGAVE.md)

