Meldingen er kryptert med XOR (indikert av oppgavetittel) med en statisk byte. CyberChef sin "XOR Brute" kan brukes til å prøve alle muligheter.

Link: https://gchq.github.io/CyberChef/

Flagg: `EGG{455e9a1473a11b406b49868e9347b9eb}`
