En kylling kommer løpende med en merkelig melding hun har skrevet ned på en lapp. Meldingen er sett i utgående trafikk fra en av Påskeharens servere mot ukjente servere på det skumle internettet. Påskeharen skal til å hente bikkja, men en litt erfaren analytiker mener dette er så enkelt at det ikke trengs.

På lappen står det: `BAYGOnV0dCR4IHB1dnIgcHAjdXF3I3V4eXd5JHhydXYjeCQjPA==`

Kan du dekode meldingen?
