Påskeharen har besøk av revisoren for å få godkjent regnskapet for 2019, og spør samtidig om han har noen gode tips til håndtering av det nylig oppståtte cyberangrepet. Dessverre må revisoren meddele at han kun har peiling på regnskap og skatteloven.

    Takk for besøket og god påske, herr revisor, sier Påskeharen i det revisoren skal til å dra.
    Selv takk, Påskehare! Nå må jeg ta en tur til påskekyllingene.
    Jaså, er du revisor for dem også?
    Javisst! Jeg er deres største felles revisor, humrer revisoren og lukker døra.

Angriperne har installert en RAT (remote administration tool) på en av Påskeharens servere. Heldigvis har Påskeharens kryptohund sniffet alle pakkene som er gått mellom serveren og angriperne, men det er uvisst om krypteringen lar seg knekke.

Det har tidligere vært spor av at angriperne har laget sin egen løsning for generering av kryptonøkler, og at de kanskje ikke har fullt så mye peiling som de burde ha.

I filen https.pcap ligger det to HTTPS-sesjoner mellom Påskeharens server og to av angripernes servere. Klarer du å dekryptere trafikken som kryptohunden har sniffet?

`Lever EGG{32-byte hex}`

FIL: [https.pcap](https.pcap)

[HINT](HINT.md)
