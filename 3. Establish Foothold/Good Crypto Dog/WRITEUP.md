## Good Crypto Dog

Oppgaven hinter til største felles divisor (greatest common divisor på engelsk). En offentlig RSA-nøkkel består av et produkt (kalt modulus) av to svære primtall og en offentlig eksponent (ofte tallet 65537). Her har altså angriperne brukt et felles primtall i to forskjellige sett RSA-nøkler, mens det andre primtallet er unikt for hver nøkkel. Dette kan skje ved feilimplementasjon eller for dårlig entropi ved nøkkelgenerering. Ved å finne største felles divisor av modulus i de offentlige nøklene har man effektivt faktorisere begge nøklenes modulus og dermed knekke nøklene.

Ideelt sett skal serverens offentlige krypteringsnøkkel kun brukes til autentisering. Da genereres det nye sesjonsnøkler for hver TLS-sesjon som utveksles på sikkert vis mellom klient og server, og disse nye nøklene brukes for kryptering av sesjonen. Hvis angriperne hadde brukt en chifferserie som genererte og utvekslet nye nøkler etter autentisering hadde vi ikke kunnet dekryptere trafikken på denne måten. (https://en.wikipedia.org/wiki/Forward_secrecy)

Mer dyptgående stoff om RSA: https://en.wikipedia.org/wiki/RSA_(cryptosystem)


Om nøkkelgenerering: https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Key_generation


Storstilt samling av offentlige RSA-nøkler og GCD av alle par av disse har vært gjort: https://eprint.iacr.org/2016/515.pdf https://research.kudelskisecurity.com/2018/10/16/reaping-and-breaking-keys-at-scale-when-crypto-meets-big-data/


## Fremgangsmåte

1. Hent ut sertifikater fra Server Hello fra begge sesjoner med wireshark. Høyreklikk på certificate: [...] -> export packet bytes. Lagre sertifikatene som henholdsvis cert1.der og cert2.der
2. Hent ut modulus fra offentlig nøkkel i begge sertifikatene:
   openssl x509 -in cert1.der -inform der -noout -modulus
   openssl x509 -in cert2.der -inform der -noout -modulus
3. Finn GCD av modulus:

```python
from fractions import gcd
from gmpy import invert

def lcm(a, b):
    return a * b / gcd(a, b)

# Private key
n1 = 0x<modulus key1>
n2 = 0x<modulus key2>
e = 65537

# Kjoer gcd:
common_p = gcd(n1, n2) # p er naa den felles faktoren i de offentlige noeklene.

# Regn ut andre verdier for privatnoekkel: p, q, d, e, exponent 1, coeff 

key1_q = n1 / common_p
key1_lambda_n = lcm(common_p - 1, key1_q - 1)
key1_d = invert(e, key1_lambda_n)
key1_e1= key1_d % (common_p-1)
key1_e2= key1_d % (key1_q-1)
key1_coeff = invert(key1_q, common_p)

key2_q = n2 / common_p
key2_lambda_n = lcm(common_p - 1, key2_q - 1)
key2_d = invert(e, key2_lambda_n)
key2_e1= key2_d % (common_p-1)
key2_e2= key2_d % (key2_q-1)
key2_coeff = invert(key2_q, common_p)

# Skriv ut noeklene i saert format (asn1) som OpenSSL kan gjoere om til normalt formatterte privatnoekler
key_text = """asn1=SEQUENCE:rsa_key
[rsa_key]
version=INTEGER:0
modulus=INTEGER:%d
pubExp=INTEGER:%d
privExp=INTEGER:%d
p=INTEGER:%d"
q=INTEGER:%d"
e1=INTEGER:%d"
e2=INTEGER:%d"
coeff=INTEGER:%d
"""

key1 = key_text % (n1, e, key1_d, common_p, key1_q, key1_e1, key1_e2, key1_coeff)
with open("key1.txt", "w") as f:
    f.write(key1)

key2 = key_text % (n2, e, key2_d, common_p, key2_q, key2_e1, key2_e2, key2_coeff)
with open("key2.txt", "w") as f:
    f.write(key1)
```

`python gcd_pub_key.py`

4. Lag privatnøkler fra parametre og gjør om til PEM-format:
   openssl asn1parse -genconf key1.txt -out privkey1.der
   openssl asn1parse -genconf key2.txt -out privkey2.der
   openssl rsa -in privkey1.der -inform der -out privkey1.pem -outform pem
   openssl rsa -in privkey2.der -inform der -out privkey2.pem -outform pem
5. Nå har vi privatnøklene som ligger på angripernes to servere. Wireshark er så elskverdig å dekryptere trafikken for oss. Først må vi legge inn nøklene:
   Wireshark: Edit -> Preferences -> Protocols -> SSL -> Add key. Legg inn kun IP, port, filnavn
6. Finn en TLS-pakke i Wireshark -> Høyreklikk -> Follow -> SSL Stream -> Flagg

## Flagg

Flagg: `EGG{20bf8eef3e416c9a67dced7be91c777d}`
