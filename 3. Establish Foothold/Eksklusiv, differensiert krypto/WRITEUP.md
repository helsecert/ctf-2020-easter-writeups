Data hentes ut fra body i HTTP-forespørsel, f. eks. med Wireshark (høyreklikk, "Follow TCP stream").

Med litt frekvensanalyse kan man finne en del riktige bokstaver og tall, deretter er det et mønster som må finnes. Det gis hint i tittelen om at det har noe med en differanse å gjøre. Ved å først trekke fra 20 (0x14) fra hver byte, og deretter gjøre XOR på hver byte med 44 (0x2c) får du riktig svar.

Det som ikke var meningen var at dette skulle misforstås dithen at det hadde noe med "Input differential" eller "Output differential" modes for XOR i CyberChef å gjøre. Dette forvirret flere deltakere.

Link 1: https://no.wikipedia.org/wiki/Frekvensanalyse_(kryptografi)

## Løsning i Python2
```python
import sys
data = sys.stdin.readline().strip().decode("hex")
print "".join(chr((ord(ch)-20)^44) for ch in data)
```

Flagg: `EGG{3199d7605c0675e56c9e055bea191e4f}`
