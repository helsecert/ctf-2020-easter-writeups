Påskeharens kryptohund har markert på dårlig kryptering. Den har sniffet pakker mellom en maskin i DMZ (172.17.0.1) og en ukjent IP-adresse (172.17.0.2) på internett.

I den ukrypterte HTTP-sesjonen, som kryptohunden vanligvis ikke ville brydd seg om, ser det nemlig ut til å være noe slags kryptert data. Heldigvis er krypteringen svært svak.

Pakkedumpen ligger i filen http.pcap. Klarer du å dekryptere innholdet?

FIL: [http.pcap](http.pcap)

[HINT](HINT.md)