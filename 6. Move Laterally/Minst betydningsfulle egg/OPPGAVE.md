Påskekyllingen ser på deg og sier "takk for hjelpa, nå har vi sikret EGGet - jeg skal se mer på filen, kanskje kan det være noe mer skjult der".

Denne påskekyllingen har også stor interesse for bilder. De beste bildene er de bildene som har masse egg i seg. Noen egg er veldig synlige, mens andre EGG kan være vanskelig å se.

Etter en stund utbryter påskekyllingen "her er det noe muffens".

Kan du finne ut hvorfor påskekyllingen mener det er noe muffens med filen, er det flere EGG der?

`Lever EGG{32-byte hex}`

Fil: [task.png](task.png)

[HINT](HINT.md)