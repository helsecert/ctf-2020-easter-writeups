from PIL import Image

# Should be a .png!!
imageToStegoCheck="EGG{b18b80d6f48da6be15103b8ce3639319}.png"

# keywords to search for. 
# Format: 
#   [keyword,length in front of keyword to display, length after keyword to display]
key_words=[
    ['EGG{',0,37]
]

# Loading image
img=Image.open(imageToStegoCheck,mode='r')
pixels=img.load()

# Searching for a binary match take possible offsets into account
def binify_search_word(word):
    bins=''
    imgWidth, imgHeigth=img.size

    for char in word:
        bins+='{0:08b}'.format(ord(char))
    return bins

# Turning finds back into strings before printing
def asciiPrint(bins,pos,foundIn):
    res=""
    for i in range(0,len(bins),8):
        res+=chr(int(bins[i:i+8],2))
    print(res+ "\tFrom: "+foundIn+"\tStarting in position: "+str(pos))

# Printing all finds
def printOccurences(word,bins,foundIn):
    searchstring=binify_search_word(word[0])
    minus_offset=word[1]*8
    plus_offset=word[2]*8
    num=bins.count(searchstring)
    while num>0:
        pos=bins.index(searchstring)
        if pos-minus_offset<0:
            from_pos=0
        else:
            from_pos=pos-minus_offset
        asciiPrint(bins[from_pos:pos+plus_offset],pos,foundIn)
        bins=bins[pos:]
        num-=1

def stegoRoutine():
    imgWidth, imgHeigth=img.size
    bins_r=''
    bins_g=''
    bins_b=''
    bins_rgb=''

    for y in range(imgHeigth):
        for x in range(imgWidth):
            pix=pixels[x,y]
            bins_r+=str(pix[0]&1)
            bins_g+=str(pix[1]&1)
            bins_b+=str(pix[2]&1)
            bins_rgb+=str(pix[0]&1)+str(pix[1]&1)+str(pix[2]&1)
            
    for word in key_words:
        searchstring=binify_search_word(word[0])
        if searchstring in bins_r:
            printOccurences(word,bins_r,'Red pixels')
        if searchstring in bins_g:
            printOccurences(word,bins_g,'Green pixels')
        if searchstring in bins_b:
            printOccurences(word,bins_b,'Blue pixels')
        if searchstring in bins_rgb:
            printOccurences(word,bins_rgb,'RGB pixels')

stegoRoutine()