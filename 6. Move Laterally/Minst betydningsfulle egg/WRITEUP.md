## Minst betydningsfulle EGG

Oppgaveoverskriften og oppgaveteksten hinter mot at noe er gjemt i et bilde.  
![Screenshot av oppgave "Minst betydningsfulle EGG"](imgs/del_2_1.png "Screenshot av oppgave 'Minst betydningsfulle EGG'")

Vi går tilbake til pcap fra HTML og åpner neste tcp-stream. Vi ser at en PNG-fil er blitt overført. Vi ser starten på headeren dens i tcp-strømmen.
![tcp-stream 3 fra pcap](imgs/del_2_2.png "tcp-stream 3 fra pcap")  
Det er flere måter å nå hente bildet ut av pcapen på. En er å sette "Follow tcp-stream" til å vise bare data overført en vei (for å ta bort litt unøvendig data), sette framvisning til "RAW" og deretter "save-as" for å lagre resulterende data.   
![Lagring av data fra tcp-strøm 2](imgs/del_2_3.png "Lagring av data fra tcp-strøm 2")  

Dette tar også med en header som har med overføringen av bildet fra serveren og gjøre, men som ikke har noe med bildet å gjøre. Vi åpner den resulterende filen i en [hexeditor](https://mh-nexus.de/en/hxd/) og sletter all data fram til PNG-headeren.  
![Sletting av unødig data i HxD](imgs/del_2_4.png "Sletting av unødig data i HxD")

Dette resulterer i en fil med bilde av et påskeegg.  
![Bilde av påskeegg, fra pcap](imgs/del_2_5.png "Bilde av påskeegg, fra pcap")  
Basert på oppgaveteksten kan vi finne neste EGG gjemt i dette bildet. Det er flere måter slikt kan gjøres på, men en ganske kjent metode er steganografi, noe overskriften også hinter mot. lsb-stego går ut på å bytte ut de "minste" bitsene i fargepikslene til bildet.
Dataen som gjemmes kan være fordelt/kodet på mange forskjellige måter. 

Det finnes mange bra verktøy for å lete etter gjemt data i bilder. Et som vi bruker som løsningsmetode her er [StegOnline](#StegOnline), som både har en praktisk [onlineversjon](https://georgeom.net/StegOnline/upload) og en [githubside](https://github.com/Ge0rg3/StegOnline) om man ønsker å sette opp selv.
Det finnes også et [docker-toolkit](https://hub.docker.com/r/dominicbreuker/stego-toolkit/) med mye stegoverktøy lagt inn. Det blir ikke brukt noe her.
Vi kan også ganske enkelt skrive verktøy selv, noe som er god øvelse, og kan lages til å løse spesifikke problemer vi har. [Se lengre ned for løsning med dette.](#egenkode)

### Løsning ved StegOnline
<a name="StegOnline"></a>

Vi laster opp bildet på StegOnline og begynner med å sjekke gjennom bit-planene etter noe som skiller seg ut. Vi ser raskt at bitplan-0 for rød er helt tomt med unntak av noe data i starten.  
![Bitplan 0 for rød og grønn for sammenligning](imgs/del_2_6.png "Bitplan 0 for rød og grønn for sammenligning")  
Dette er et klart tegn på at noe er gjort med bitplan 0.

StegOnline har også verktøy for å forsøke å hente ut data fra forskjellige bitplan, enten enkeltvis eller kombinert.

Vi henter ut fra bitplan 0 for rød og får ut flagget.  
![Flagget ligger i bitplan 0 for rød](imgs/del_2_7.png "Flagget ligger i bitplan 0 for rød")  


### Løsning med egen kode
<a name="egenkode"></a>  
PIL biblioteket i python kan brukes for manipulasjon av PNG-bilder. Skriptet lsb_stego.py bruker PIL til å hente ut LSB-bits fra RGB og lagre disse for R, G, B og RGB, før det søker gjennom hver av disse etter treff på match fra nøkkelord.  
Vi bruker 
`EGG{`  
Som nøkkelord, og ved treff tar vi med output fra treff + 37 bytes. Når vi kjører dette skriptet får vi treff på et EGG i de røde pikslene fra offset 0.

![Resultat av stego_skript](imgs/2020-05-03_20-38.png "Resultat av stego_skript")