Påskeharen har bestilt seg et nytt fysisk token for å kunne koble seg på hjemmekontorløsningen. Mens han likevel er innom kontoret for å hente dette, logger han seg også inn på en server for å se over det siste arbeidet gjort av noen påskekyllinger. Han kommer med et uhell til å kjøre et ukjent program med et mystisk navn som han mistenker noen uvedkomne kan ha lagt igjen.

Straks Påskeharen innser hva han har gjort kontakter han sikkerhetsteamet sitt og den virtuelle serveren tas ut av nettverket. Her var det heldigvis ikke noen veldig sensitive data, men vi vet fortsatt ikke hvilke andre servere angriperne har vært innom.

Det har igjen vist seg at angriperne har beveget seg mellom flere av Påskeharens servere. Denne spesifikke serveren blir ofte brukt av påskekyllinger til å utføre vanlige administrative oppgaver fra deres egne Kali-maskiner, og angriperne har brukt en kompromittert bruker til å legge igjen skadevaren på serveren.

For å kunne kaste angriperne helt ut av nettverket trenger Påskeharens sikkerhetsteam IP-adressen angriperen koblet seg til serveren fra. For å kunne finne dette, har de hentet ut sysmon- og security-loggen fra serveren. Loggene ligger i sysmon2.zip. Klarer du å finne ut hvilken IP adresse angriperen koblet til fra?

Lever svaret som `EGG{md5sum(IP-adresse)}`

Fil: [angriperidentifikasjon.zip](angriperidentifikasjon.zip)

[HINT](HINT.md)