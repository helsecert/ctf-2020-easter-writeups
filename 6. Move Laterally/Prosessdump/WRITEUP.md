# Oppgave 1
## Løsning med Event Viewer
Om vi ser på dataen vi har kan vi med en gang se at vi har 24966 events. For å bedre finne ut av hva vi har kan vi kjapt rulle oss nedover, og vi ser følgende event IDer:
* 1 - Process create
* 11 - File created
* 13 - Registry value set

For å bekrefte at vi ikke har oversett noen event IDer, kan vi filtrere bort events med disse IDene, og da ser vi at ingenting dukker opp:

![Filtrert eventlog](images/o1p1.png)

![Ingen andre IDer](images/o1p2.png)



Vi vet fra oppgaveteksten at påskekyllingene bruker maskinen til å trene seg på ProcDump, noe vi også ser igjen i dataen.

![Procdump blir brukt](images/o1p3.png)



Det vi er ute etter er et tegn på at en av påskekyllingene har blitt kompromittert. Siden vi ikke har fått noe mere informasjon om hva det er som er gjort, kan vi se om vi finner noe som kan gjøres med procdump. Ved å søke på procdump på google, kan vi se hint til mimikatz og lsass.exe:

![Mimikatz relatert til procdump](images/o1p4.png)



Etter å ha lest oss opp vet vi at om man procdumper lsass.exe, kan man hente ut windows-credentials.

For å finne ut om dette er noe som har skjedd her, bruker vi Event Viewers "Find" funksjonalitet:

![Den skylige er funnet](images/o1p5.png)



Det vi kan se er at kylling404 har brukt procdump på lsass, noe som er et tegn på at noe muffens foregår.
Vi henter md5sum av kylling404 som blir: 40168bd00430816c6cbb48e47698a17e

Svaret er da EGG{40168bd00430816c6cbb48e47698a17e}, som var riktig. Dersom dette ikke var riktig, måtte vi ha lett videre etter mistenkelige kommandoer som ble kjørt av påskekyllingene.
