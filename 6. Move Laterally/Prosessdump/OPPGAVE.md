Påskekyllingene er godt på vei til å bli flinke cyberoperatører. Daglig logger hundrevis av dem seg på en av Påskeharens sentrale servere, og øver seg på å monitorere prosesser på serveren med ProcDump. De er ikke så flinke til å skrive ennå, så det blir ofte skrivefeil i kommandoene.

Dessverre har Påskeharen blitt informert av sikkerhetssjefen sin om at en angriper har kommet seg inn i systemet, og har beveget seg mellom flere forskjellige servere. En av disse er serveren påskekyllingene bruker. Her er det grunn til å tro at en av påskekyllingene sin brukerkonto er blitt tatt over av angriperene, som har brukt den til å gjøre minnedump av en prosess som ofte har passordhasher lagret i minnet.

Heldigvis vet Påskeharens driftsteam at sikkerhetslogging er viktig, og har installert Sysmon på serveren.

I filen sysmon1.zip ligger sysmon-loggen fra serveren. Den samme loggen er vedlagt både i proprietært evtx-format og som XML, du kan bruke det du selv foretrekker. Klarer du å finne ut hvilken påskekylling som har blitt tatt over av angriperen?

Svaret er `EGG{md5sum(brukernavn)}`

Fil: [prosessdump.zip](prosessdump.zip)

[HINT](HINT.md)