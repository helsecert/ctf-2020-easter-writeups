Oppgavene for `Move laterally` er delt i sporene `File transfer` og `Sysmon`

Oppgavene for `File transfer` er tiltenkt å gjøres i rekkefølgen:

- [HTML](HTML/OPPGAVE.md)
- [Minst betydningsfulle egg](Minst\ betydningsfulle\ egg/OPPGAVE.md)
- [Skjult filnavn](Skjult\ filnavn/OPPGAVE.md)
- [Vertikal forskyvning](Vertikal\ forskyvning/OPPGAVE.md)


Oppgavene for `Sysmon` er tiltenkt å gjøres i rekkefølgen:

- [Prosessdump](Prosessdump/OPPGAVE.md)
- [Angriperidentifikasjon](Angriperidentifikasjon/OPPGAVE.md)
