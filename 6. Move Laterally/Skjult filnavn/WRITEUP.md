## Skjult filnavn

![Oppgavetekst, skjult filnavn](imgs/2020-05-02_18-48.png "Oppgavetekst, skjult filnavn")  
![Hint: Passorder er ikke langt unna](imgs/2020-04-27_20-58_4.png "Hint: Passorder er ikke langt unna")  

Vi åpner vedlagt pcap og ser at det er mye SMB-overføringer. Mens vi scroller gjennom overføringene ser vi en stor smb-overføring av filen toolz.7z.
En kjapp scroll gjennom pakkedataen viser en streng på slutten som kan se ut som base64-data.  
![pcap med smb overføring av toolz.7z, med base64-streng på slutten](imgs/2020-05-02_18-53.png "pcap med smb overføring av toolz.7z, med base64-streng på slutten")  

Vi dekoder dataen i [cyberchef](https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)&input=Y0ZCWFdVOUpUV3QyY1ZSTGQyeFJaelpuWTNOTVJISnZSbFJ2Y0dFd1VERT0) og ser at det er base64, men ikke noe som gir mening.  
![Base64-streng dekoded i cyberchef](imgs/2020-05-02_18-55.png "Base64-streng dekoded i cyberchef")  
Vi kunne ha carvet toolz.7z fra pcapen, men wireshark har innebygget funksjonalitet som lar oss gjøre dette enda lettere.  
File-->Export Objects-->SMB i Wireshark gir følgende vindu.  
![Wireshark export smb-objects-dialog](imgs/2020-05-02_19-00.png "Wireshark export smb-objects-dialog")  
Toolz.7z viser seg å være kryptert. 7z er en fin protokoll til å sende sensitiv informasjon på siden den bruker bra kryptering i tillegg til at filnavn er krypterte.   
![Toolz.7z krever passord](imgs/2020-05-02_19-02.png "Toolz.7z krever passord")  
Vi tester med base64-strengen som var slengt med på slutten av overføringen av toolz.7z. Den fungerer og lar oss pakke ut en .png med EGGet som filnavn.
![Passordet fungerer og vi får opp 7z-filen med filen med EGGet som navn](imgs/2020-05-02_19-03.png "Passordet fungerer og vi får opp 7z-filen med filen med EGGet som navn")
![EGGet](imgs/2020-05-02_19-03_1.png "EGGet")  


