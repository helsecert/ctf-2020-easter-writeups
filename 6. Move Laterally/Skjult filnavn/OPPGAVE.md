Angriper forsøker nå sideveis bevegelse. Denne teknikken brukes for å systematisk flytte seg rundt i nettverket og infrastrukturen for å søke og lete etter hva enn man ønsker å oppnå. Dette kan for eksempel være å stjele påskeegg, passord eller for å eksfiltrere data ut av nettverket.

En påskekylling holder på med analyse av en overføring som er fanget opp på ett av de avanserte systemene til påskeharen. Det er overført en stor mengde data mot SRV-HEMMELIG01. Drifts-kyllingene har logget på serveren, men finner ingen nye eller unormale filer.

Kan du finne filnavnet?

`Lever EGG{32-byte hex}`

Fil: [filoverfring.pcap](filoverfring.pcap)

[HINT](HINT.md)
