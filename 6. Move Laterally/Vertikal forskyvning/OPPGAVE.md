Påskeharen visste ikke at SRV-HEMMELIG01 hadde brannmuråpninger som tillater SMB. Ved sist brannmurrevisjon ble han forsikret om at alle brannmuråpningene var i tråd med risikovurderinger og dokumentasjon.

Den eksterne konsulenten hadde sagt at det var et veldig godt driftet nettverk. Etter endt oppdrag meddelte konsulenten at han egentlig var klar til å dra hjem og innta horisontalen, men at her måtte man kjøre vertikalen først. Han mumlet også noe om forskyvning i lasta. Påskeharen skjønte ikke helt hva han snakket om, men det var ikke så nøye. Brannmurreglene var gode og konsulenttimene rant ikke lenger ut av bankkontoen.

Nå begynner Påskeharen å lure på om han hadde rent mel i posen, eller om det var ugler i mosen.

Det virker som angriper kun har overført bildet av et påskeegg, kan det stemme da? Finner du flere EGG?

`Lever EGG{32-byte hex}`

Fil: [task.png](task.png)

[HINT](HINT.md)