## Vertiakl forsykvning
![Oppgavetekst til vertikal forskyvning](imgs/2020-05-03_01-59.png "Oppgavetekst til vertikal forskyvning")

![Hint Vertikal forskyvning](imgs/2020-05-03_15-18.png "Hint Vertikal forskyvning")
 
Vi begynner som i [minst betydningsfulle egg](../Minst\ betydningsfulle\ egg/writeup.md). Vi sjekker file, strings og binwalk uten at noen av disse gir oss noe fornuftig om filen. Vi laster den deretter opp til [stegonline](https://georgeom.net/StegOnline/image) og går gjennom fargenanser og alle bitplan uten at vi greier å plukke ut noe spesielt.  
![Stegonline viser ikke noe spesielt på i noen av bitplanene i bildet.](imgs/2020-05-03_15-17.png "Stegonline viser ikke noe spesielt på i noen av bitplanene i bildet.")
Siden hintet sier at den er ganske lik [minst betydningsfulle egg](../Minst\ betydningsfulle\ egg/writeup.md) men stegonline ikke enkelt peker oss videre går vi tilbke til skriptet vi brukte i forrige stegooppgave for å hente ut EGGet.  

Når vi kjører det umodifisert mot EGG{946ad89061dc82d12249d3195853c685}.png gir det ingen treff.
P.T. går lsb_stego.py gjennom pikslene som man leser en bok. Rad for rad, venstre mot høyre. Hintene tyder imidlertid på at vi ikke skal angripe oppgaven horisontalt, men vertikalt. Vi legger derfor til at lsb_stego.py i tillegg til y-x gjennomgang også går gjennom x-y en gang.

Dette resulterer i at vi finner flagget.  
![Vi finner flagget på offset 164955 med xy som søkeretning](imgs/2020-05-03_20-54.png "Vi finner flagget på offset 164955 med xy som søkeretning")  
Merk at offset er 164955. For dette er både mot slutten av filen, og en offset som ikke går opp i 8. Her kommer delen av hintet som snakket om forskyvning av lasten inn. Hadde vi ikke tatt høyde for dette via binærsøk ville vi ikke funnet flagget  her.
