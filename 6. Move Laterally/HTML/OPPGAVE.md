Analysesentret fanget opp en alarm i natt. Den trigget på at det er overført en stor mengde data mot serveren srv-dmz12.ctf. For tre uker siden var det også en tilsvarende alarm på samme server, men denne alarmen viste seg å være en falsk positiv.

En av påskekyllingene har begynt å se på PCAP filen, men har ikke funnet noe enda. Kan du hjelpe til?

Lever EGG{32-byte hex}

Fil: [nginx.pcap](nginx.pcap)