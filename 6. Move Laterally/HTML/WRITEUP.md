## HTML

Enkleste oppgave under Move Laterallly / File Transfer ber oss om å se hva vi kan finne ut av en pcap.  
![Oppgaveinformasjon](imgs/del_1_1.png "Oppgaveinformasjon")

Vi laster ned og åpner filen med [Wireshark](https://www.wireshark.org/). Vi ser at den består av tcp-trafikk mellom to IPer.
Vi høyreklikker og velger "follow tcp stream" for å se hva som har gått.
![Høyreklikk i Wireshark for å følge TCP-strømmen](imgs/del_1_2.png "Høyreklikk i Wireshark for å følge TCP-strømmen")

Vi ser at tcp-stream 0 er en get-request mot noe som kan se ut som en bildeopplastingsserver.  
![tcp-stream er en get-request med svar mot noe som identifiserer seg som en bildeopplastingsserver](imgs/del_1_3.png "tcp-stream er en get-request med svar mot noe som identifiserer seg som en bildeopplastingsserver")  
Vi åpner og følger tcp-stream 1, og ser at dette er en GET-request for et opplastingsskjema, vi ser at EGGet ligger inne her som et hidden-felt.   
![tcp-stream 1 viser et opplastingsskjema, inkludert EGGet i en hidden-felt](imgs/del_1_4.png  "tcp-stream 1 viser et opplastingsskjema, inkludert EGGet i en hidden-felt")